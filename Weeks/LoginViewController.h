//
//  LoginViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 04.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

+ (void)getFacebookAccessTokenFromViewController:(UIViewController *)controller completion:(void (^)(BOOL result))completion;
@end
