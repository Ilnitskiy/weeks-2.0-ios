//
//  MentorCommentTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 30.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "MentorCommentTableViewCell.h"

@implementation MentorCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
