//
//  GroupModel.m
//  Weeks
//
//  Created by Max Ilnitskiy on 24.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "GroupModel.h"

@implementation GroupModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        _ID = [[dictionary objectForKey:@"id"] intValue];
        _name = [dictionary objectForKey:@"name"];
        _course = [[dictionary objectForKey:@"course"] intValue];
    }
    
    return self;
}
@end
