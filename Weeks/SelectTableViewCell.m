//
//  SelectTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "SelectTableViewCell.h"

@implementation SelectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
