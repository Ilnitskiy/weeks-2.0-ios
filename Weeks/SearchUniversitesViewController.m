//
//  SearchUniversitesViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 20.11.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "SearchUniversitesViewController.h"
#import "FBSDKCoreKit.h"
#import "LoginViewController.h"
#import "FBSDKLoginKit.h"
#import "VKSdk.h"
#import "NetworkManager.h"
#import "AFNetworking.h"
#import "CreateGroupViewController.h"
#import "TutorialUniverChooseViewController.h"

#import "UniversitetModel.h"
#import "FackultetModel.h"
#import "GroupModel.h"

#import "Analytics.h"

@interface SearchUniversitesViewController () <UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NetworkManager *mnetworkManager;
@property (strong, nonatomic) AFHTTPSessionManager *manager;

- (IBAction)addButton:(id)sender;
- (IBAction)changeuser:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *tutorialView;

@property (strong, nonatomic) NSString *myId;
@property (strong, nonatomic) NSString *kurs;
@property (strong, nonatomic) NSDictionary *myTable;
@property (strong, nonatomic) NSArray *myTableectionTitles;
@property (strong, nonatomic) NSString *restID;

@end

@implementation SearchUniversitesViewController
{
    NSArray *univeritetsArray;
    NSArray *facultetsArray;
    NSArray *groupsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *tutName = @"tutorialUniver";
    if (![usDef objectForKey:tutName]) {
        TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.tutToDel = tutName;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    
    self.restID = self.restorationIdentifier;
    self.mnetworkManager = [[NetworkManager alloc]init];
    //    NSString *fackID = [usDef objectForKey:@"Facultet ID"];
    //    NSString *univerID = [usDef objectForKey:@"Univer ID"];
    
    if ([self.restID isEqualToString:@"universitet"]) {
        [self.mnetworkManager getUnibersWithComplection:^(NSArray* univers) {
            univeritetsArray = univers;
            [self makeUniversDictionary:univers];
            [indicator stopAnimating];
        } failure:^(NSError *error) {
            [indicator stopAnimating];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Сервер не доступний" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }];
    }else if ([self.restID isEqualToString:@"facultet"]) {
        NSString *unId = [usDef objectForKey:@"Univer ID"];
        [self.mnetworkManager getFackultetsWithUniverID:unId completion:^(NSArray *facks) {
            facultetsArray = facks;
            [self makeFcultetsDictionary:facks];
            [indicator stopAnimating];
        } failure:^(NSError *error) {
            [indicator stopAnimating];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Сервер не доступний" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }];
    }else if ([self.restID isEqualToString:@"group"]) {
        
        NSString *fId = [usDef objectForKey:@"Facultet ID"];
        [self.mnetworkManager getGroupsWithFacksID:fId completion:^(NSArray *groups) {
            groupsArray = groups;
            [self makeGroupsDictionary:groups];
            [indicator stopAnimating];
        } failure:^(NSError *error) {
            [indicator stopAnimating];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Сервер не доступний" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }];
    }
    
    
//    if (![FBSDKAccessToken currentAccessToken]) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
//        
//        [self presentViewController:vc animated:YES completion:nil];
//    }
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    NSDictionary *universitisTemp = [[NSDictionary alloc]init];
    
    universitisTemp = @{
                        @"А":@[@"ААУ",@"АМУ"],
                        @"Д":@[@"ДЕТУТ",@"ДНУ"],
                        @"К":@[@"КНЕУ",@"КНУ",@"КНТУ",@"КНУБА"],
                        @"Н":@[@"НТУУ КПИ",@"НАУ"],
                        @"Т":@[@"ТПУ",@"ТГДУ"],
                        };
    
    NSDictionary *facultetsTemp = [[NSDictionary alloc]init];
    facultetsTemp = @{
                      @"А":@[@"АНФ",@"АББКФ"],
                      @"Д":@[@"ДТТПФ",@"ДСФ"],
                      @"К":@[@"КПФ",@"КЕВФ",@"КПФ"],
                      @"Н":@[@"НВФ",@"НФ"],
                      @"Т":@[@"ТЕФ",@"ТГДУф"],
                      @"Ф":@[@"ФЕЛ",@"ФІОТ",@"ФЛ",@"ФФП",@"ФСП"],
                      };
    
    NSDictionary *groupsTemp = [[NSDictionary alloc]init];
    groupsTemp  = @{
                    @"А":@[@"АО-23",@"АО-12"],
                    @"Б":@[@"БК-21",@"БК-33"],
                    @"С":@[@"СП-15",@"СС-12",@"СС-13"],
                    @"Н":@[@"НП-23",@"НП-24"],
                    };
    
    if ([self.restID isEqualToString:@"universitet"]) {
        //  self.myTable = universitisTemp;
    }else if ([self.restID isEqualToString:@"facultet"]) {
        //  self.myTable = facultetsTemp;
    }else if ([self.restID isEqualToString:@"group"]) {
        // self.myTable = groupsTemp;
    }
    else if ([self.restID isEqualToString:@"kurs"]) {
        [indicator stopAnimating];
        self.myTable = nil;
        self.myTableectionTitles = [NSArray arrayWithObjects:@"1 Курс",@"2 Курс",@"3 Курс",@"4 Курс",@"5 Курс",@"6 Курс", nil];
    }
    
    if (![self.restID isEqualToString:@"kurs"]) {
        self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Пошук університетів"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([self.restID isEqualToString:@"kurs"]) {
        return 1;
    }
    return [self.myTableectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self.restID isEqualToString:@"kurs"]) {
        return nil;
    }
    return [self.myTableectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([self.restID isEqualToString:@"kurs"]) {
        return [self.myTableectionTitles count];
    }
    else {
        NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:section];
        NSArray *sectionAnimals = [self.myTable objectForKey:sectionTitle];
        return [sectionAnimals count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if ([self.restID isEqualToString:@"kurs"]) {
        NSString *universitet = [self.myTableectionTitles objectAtIndex:indexPath.row];
        cell.textLabel.text = universitet;
        
        return cell;
    }
    else {
        NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
        
        UniversitetModel* model = [[UniversitetModel alloc]init];
        model = [sectionUnivers objectAtIndex:indexPath.row];
        NSString *universitet =model.name;
        
        cell.textLabel.text = universitet;
        
        if ([self.restID isEqualToString:@"universitet"]) {
            cell.detailTextLabel.text = model.city;
        }
        
        return cell;
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if ([self.restID isEqualToString:@"kurs"]) {
        return nil;
    }
    return self.myTableectionTitles;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    
    if ([self.restID isEqualToString:@"universitet"]) {
        UniversitetModel *univer = [[UniversitetModel alloc]init];
        univer = [sectionUnivers objectAtIndex:indexPath.row];
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)univer.ID] forKey:@"Univer ID"];
        //self.myId = [NSString stringWithFormat:@"%i",univer.ID];
        
    }else if ([self.restID isEqualToString:@"facultet"]) {
        FackultetModel *fack = [[FackultetModel alloc]init];
        fack = [sectionUnivers objectAtIndex:indexPath.row];
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)fack.ID] forKey:@"Facultet ID"];
        //self.myId = [NSString stringWithFormat:@"%i",fack.ID];
        
    }else if ([self.restID isEqualToString:@"group"]) {
        GroupModel *grp = [[GroupModel alloc]init];
        grp = [sectionUnivers objectAtIndex:indexPath.row];
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)grp.ID] forKey:@"Group ID"];
        [usDef setObject:[NSString stringWithFormat:@"%@",grp.name] forKey:@"Group name"];
        [usDef removeObjectForKey:@"ScheduleG"];
        [usDef removeObjectForKey:@"Schedule"];
        
        if ([usDef objectForKey:@"User INFO"]) {
            
            NSDictionary *stdntData = [NSDictionary dictionaryWithDictionary:[usDef objectForKey:@"User INFO"]];
            
            [self.mnetworkManager createNewStudentWithGroupID:[NSString stringWithFormat:@"%li",(long)grp.ID] andName:[stdntData objectForKey:@"name"] andProfile:[stdntData objectForKey:@"profile"]  andProfile_type:[stdntData objectForKey:@"profileType"] sex:[stdntData objectForKey:@"sex"] token:[stdntData objectForKey:@"token"] token_type:[stdntData objectForKey:@"tokenType"] photo:[stdntData objectForKey:@"photo"] withCompletion:^(NSInteger studentID) {
                
                [usDef setObject:[NSString stringWithFormat:@"%li",(long)studentID] forKey:@"Student ID"];
                [usDef synchronize];
            } failure:^(NSError *error) {
                
            }];
        }
        else {
            
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logOut];
            [VKSdk forceLogout];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
            
            [self presentViewController:vc animated:YES completion:nil];
            
        }
        
    }
    else if ([self.restID isEqualToString:@"kurs"]) {
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)indexPath.row+1] forKey:@"Kurs"];
        //self.kurs = [NSString stringWithFormat:@"%li", indexPath.row+1];
        
    }
    
    [usDef synchronize];
}

-(void)makeUniversDictionary: (NSArray*)beData {
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (UniversitetModel *univer in beData) {
        
        NSString *firstLetter = [univer.name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSArray *arrayToFilter = univeritetsArray ? : groupsArray ? : facultetsArray;
    
    if (searchText.length) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@", searchText];
        
        NSArray *filteredArray = [arrayToFilter filteredArrayUsingPredicate:bPredicate];
        
        arrayToFilter = filteredArray;
    }
    
    if (univeritetsArray) {
        [self makeUniversDictionary:arrayToFilter];
    } else if (groupsArray) {
        [self makeGroupsDictionary:arrayToFilter];
    } else if (facultetsArray) {
        [self makeFcultetsDictionary:arrayToFilter];
    }
}

-(void)makeFcultetsDictionary: (NSArray*)beData {
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (FackultetModel *univer in beData) {
        
        NSString *firstLetter = [univer.name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

-(void)makeGroupsDictionary: (NSArray*)beData {
    
    NSMutableArray *kursGroups = [[NSMutableArray alloc]init];
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSInteger kurs = [[usDef objectForKey:@"Kurs"]intValue];
    
    for (GroupModel *group in beData) {
        if (group.course == kurs) {
            [kursGroups addObject:group];
        }
    }
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (GroupModel *univer in kursGroups) {
        
        NSString *firstLetter = [univer.name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:univer];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)addButton:(id)sender {
    if ([self.restID isEqualToString:@"facultet"]) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Новий факультет"
                                                                                  message:@"Введіть назву факультету"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Наприклад: ФІОТ";
            textField.textColor = [UIColor blackColor];
            textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleNone;
            textField.secureTextEntry = NO;
            textField.keyboardType = UIKeyboardTypeDefault;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Зберегти" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * mailfield = textfields[0];
            NSLog(@"%@",mailfield.text);
            NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
            NSString *univerID = [usDef objectForKey:@"Univer ID"];
            if (mailfield.text) {
                [self.mnetworkManager createFavultetWithUniverID:univerID andName:mailfield.text withCompletion:^(NSInteger fackID) {
                    if (fackID && fackID!=0) {
                        [usDef setObject:[NSString stringWithFormat:@"%li",(long)fackID] forKey:@"Facultet ID"];
                        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                        CreateGroupViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addGroup"];
                        
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                } failure:^(NSError *error) {
                    
                }];
            }
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Відмінити" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (IBAction)changeuser:(id)sender {
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    [VKSdk forceLogout];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    [usDef removeObjectForKey:@"User INFO"];
    [usDef synchronize];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
    
    [self presentViewController:vc animated:YES completion:nil];
}
@end
