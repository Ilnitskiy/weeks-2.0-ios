//
//  ScheduleViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 04.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "ScheduleViewController.h"
#import "ScheduleTableViewCell.h"
#import "NetworkManager.h"
#import "ScheduleModel.h"
#import "LoginViewController.h"
#import "FBSDKLoginKit.h"
#import "FBSDKCoreKit.h"
#import "VKSdk.h"
#import <PDTSimpleCalendarViewController.h>

@interface ScheduleViewController () <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,VKSdkDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NetworkManager *mnetworkManager;

@property (weak, nonatomic) IBOutlet UILabel *pnNum;
@property (weak, nonatomic) IBOutlet UILabel *vtNum;
@property (weak, nonatomic) IBOutlet UILabel *srNum;
@property (weak, nonatomic) IBOutlet UILabel *chtNum;
@property (weak, nonatomic) IBOutlet UILabel *ptNum;
@property (weak, nonatomic) IBOutlet UILabel *sbNum;
@property (weak, nonatomic) IBOutlet UILabel *pn;
@property (weak, nonatomic) IBOutlet UILabel *vt;
@property (weak, nonatomic) IBOutlet UILabel *sr;
@property (weak, nonatomic) IBOutlet UILabel *cht;
@property (weak, nonatomic) IBOutlet UILabel *pt;
@property (weak, nonatomic) IBOutlet UILabel *sb;
@property (weak, nonatomic) IBOutlet UIView *sbInd;
@property (weak, nonatomic) IBOutlet UIView *ptInd;
@property (weak, nonatomic) IBOutlet UIView *chtInd;
@property (weak, nonatomic) IBOutlet UIView *srInd;
@property (weak, nonatomic) IBOutlet UIView *vtInd;
@property (weak, nonatomic) IBOutlet UIView *pnInd;

@property (strong,nonatomic) PDTSimpleCalendarViewController *cvc;

@property (strong, nonatomic) NSArray *scheduleArray;
@property (strong, nonatomic) NSArray *datesOfWeek;
@property (strong, nonatomic) NSDictionary *schduleDictionary;

@property UISwipeGestureRecognizer *leftswipe;
@property UISwipeGestureRecognizer *rightswipe;

@property NSInteger dayForIntroduse;


- (IBAction)sbBtn:(id)sender;
- (IBAction)ptBtn:(id)sender;
- (IBAction)chtBtn:(id)sender;
- (IBAction)srBtn:(id)sender;
- (IBAction)vtBtn:(id)sender;
- (IBAction)pnBtn:(id)sender;

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];

//    if (![usDef objectForKey:@"User INFO"]) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
//        
//        [self presentViewController:vc animated:YES completion:nil];
//    } else
//    if (![usDef objectForKey:@"Schedule"]) {
//        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"navigationToChoseUniversity"] animated:NO completion:nil];
//    }
    
    self.navigationItem.title = @"Розклад";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:85.0/255.0 blue:115.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.cvc = [PDTSimpleCalendarViewController new];
    NSArray *events = [NSArray arrayWithArray:[usDef objectForKey:@"Schedule"]];
    
    NSMutableArray *subjcts = [[NSMutableArray alloc]init];
    NSMutableArray *elements = [NSMutableArray array];
    for (NSDictionary *dictionary in events) {
        
        ScheduleModel *sm =
        [[ScheduleModel alloc] initWithDictionary:dictionary];
        [elements addObject:sm];
        [subjcts addObject:sm.Subject];
        
    }
    [usDef setObject:[subjcts copy] forKey:@"Subjects"];
    [usDef synchronize];
//    ScheduleModel *sm =
//    [[ScheduleModel alloc] initWithDictionary:[events objectAtIndex:0]];
//    NSArray *array = [NSArray arrayWithArray:sm.Dates];
//    
    if (!self.dateToScheduleShow) {
        self.dateToScheduleShow = [NSDate date];
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:self.dateToScheduleShow];
    NSInteger weekday = [comps weekday];
    
    NSArray *dates = [self getDatesForAllWeek];
    self.datesOfWeek = [NSArray arrayWithArray:dates];
    self.dayForIntroduse = weekday;
    
    self.pnNum.text = [[dates objectAtIndex:0] substringFromIndex:8];
    self.vtNum.text = [[dates objectAtIndex:1] substringFromIndex:8];
    self.srNum.text = [[dates objectAtIndex:2] substringFromIndex:8];
    self.chtNum.text = [[dates objectAtIndex:3] substringFromIndex:8];
    self.ptNum.text = [[dates objectAtIndex:4] substringFromIndex:8];
    self.sbNum.text = [[dates objectAtIndex:5] substringFromIndex:8];
    
    
    
    [self castomisation:weekday];
    [self nowIntroducingToColor:weekday];
    
    NSMutableDictionary *dictionaryWithDates = [NSMutableDictionary dictionary];
    for (int i=0; i<[dates count]; i++) {
        NSString *dateInWeek = [dates objectAtIndex:i];
        NSMutableArray *eventsForDate = [NSMutableArray array];
        for (ScheduleModel *sm in elements) {
            NSArray *datesOfEvent = [NSArray arrayWithArray:sm.Dates];
            for (NSString *dateInDates in datesOfEvent) {
                if ([dateInWeek isEqualToString:dateInDates]) {
                    [eventsForDate addObject:sm];
                    [dictionaryWithDates setObject:eventsForDate forKey:dateInWeek];
                }
            }
        }
    }
    
    self.schduleDictionary = [NSDictionary dictionaryWithDictionary:[dictionaryWithDates copy]];
    
    self.leftswipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRecognizer:)];
    self.leftswipe.delegate = self;
    self.leftswipe.direction = UISwipeGestureRecognizerDirectionLeft;
    
    self.rightswipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeRecognizer:)];
    self.rightswipe.delegate = self;
    self.rightswipe.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.tableView addGestureRecognizer: self.leftswipe];
    [self.tableView addGestureRecognizer: self.rightswipe];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    
    if (![usDef objectForKey:@"User INFO"]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
        [VKSdk forceLogout];
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        
        [self presentViewController:vc animated:YES completion:nil];
    } else
        if (![usDef objectForKey:@"Schedule"]) {
            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"navigationToChoseUniversity"] animated:NO completion:nil];
        }

}

- (IBAction)showCalendarButtonAction:(id)sender {
    //cvc.delegate = self;
    
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:self.cvc];
    [navVC.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
    navVC.navigationBar.tintColor = [UIColor darkGrayColor];
    [navVC.navigationBar setBarTintColor:[UIColor whiteColor]];//[UIColor colorWithRed:99.0/255.0 green:139.0/255.0 blue:165.0/255.0 alpha:1.0]];
    [navVC.navigationBar setTranslucent: NO];
    
    self.cvc.navigationItem.title = @"Календар";
    self.cvc.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Пари" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonAction)];
    
    self.cvc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад" style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonAction)];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

- (void)closeButtonAction {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)doneButtonAction {
    [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
        self.dateToScheduleShow = self.cvc.selectedDate;
        [self viewDidLoad];
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)todayDate {
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitWeekOfYear;
    NSDateComponents *components = [calendar components:units fromDate:today];
    
    NSInteger m = [components month];
    NSInteger d = [components day];
    
    NSString *mounth;
    switch (m) {
        case 1:
            mounth = @"січня";
            break;
            
        case 2:
            mounth = @"лютого";
            break;
            
        case 3:
            mounth = @"березня";
            break;
            
        case 4:
            mounth = @"квітня";
            break;
            
        case 5:
            mounth = @"травня";
            break;
            
        case 6:
            mounth = @"червня";
            break;
            
        case 7:
            mounth = @"липня";
            break;
            
        case 8:
            mounth = @"серпня";
            break;
            
        case 9:
            mounth = @"вересня";
            break;
            
        case 10:
            mounth = @"жовтня";
            break;
            
        case 11:
            mounth = @"листопада";
            break;
            
            
        default:
            mounth = @"грудня";
            break;
    }
    
    NSString *dateToReturn = [NSString stringWithFormat:@"%li %@",(long)d,mounth];
    
    return  dateToReturn;
}

-(NSArray*)sortingOfEventsArray:(NSArray*)arrayToSort {
    NSMutableArray *sortedArray = [NSMutableArray array];
    for (int i = 1; i<7; i++) {
        
        for (ScheduleModel *sm in arrayToSort) {
            NSInteger numOfPara = [sm.Number integerValue];
            if (numOfPara == i)
            {
                [sortedArray addObject:sm];
                
            }
        }
    }
    return [sortedArray copy];
}

-(void)castomisation:(NSInteger)weekday {
    self.pn.text = @"ПН";
    self.vt.text = @"ВТ";
    self.sr.text = @"СР";
    self.cht.text = @"ЧТ";
    self.pt.text = @"ПТ";
    self.sb.text = @"СБ";
    
    NSString *today = [NSDateFormatter localizedStringFromDate:[NSDate date]
                                                     dateStyle:NSDateFormatterShortStyle
                                                     timeStyle:NSDateFormatterFullStyle];
    NSString *dateInSelf = [NSDateFormatter localizedStringFromDate:self.dateToScheduleShow
                                                     dateStyle:NSDateFormatterShortStyle
                                                     timeStyle:NSDateFormatterFullStyle];
    
    today = [today substringToIndex:8];
    dateInSelf = [dateInSelf substringToIndex:8];
    
    if ([today isEqualToString:dateInSelf]) {
        
        switch (weekday) {
            case 2:
            {
                self.pn.text = @"сьогодні";
                break;
            }
            case 3:
            {
                self.vt.text = @"сьогодні";
                break;
            }
            case 4:
            {
                self.sr.text = @"сьогодні";
                break;
            }
            case 5:
            {
                self.cht.text = @"сьогодні";
                break;
            }
            case 6:
            {
                self.pt.text = @"сьогодні";
                break;
            }
            case 7:
            {
                self.sb.text = @"сьогодні";
                break;
            }
                
            default:
                break;
        }
    }

}

-(void) nowIntroducingToColor:(NSInteger)dayOfWeek {
    UIColor *selectedColor = [UIColor colorWithRed:162.0/255.0 green:104.0/255.0 blue:41.0/255.0 alpha:1.0];
    UIColor *dateUnselectColor = [UIColor colorWithRed:12.0/255.0 green:12.0/255.0 blue:12.0/255.0 alpha:1.0];
    UIColor *dayUnselectColor = [UIColor colorWithRed:138.0/255.0 green:136.0/255.0 blue:136.0/255.0 alpha:1.0];
    
    self.pn.textColor = dayUnselectColor;
    self.vt.textColor = dayUnselectColor;
    self.sr.textColor = dayUnselectColor;
    self.cht.textColor = dayUnselectColor;
    self.pt.textColor = dayUnselectColor;
    self.sb.textColor = dayUnselectColor;
    
    self.pnNum.textColor = dateUnselectColor;
    self.vtNum.textColor = dateUnselectColor;
    self.srNum.textColor = dateUnselectColor;
    self.chtNum.textColor = dateUnselectColor;
    self.ptNum.textColor = dateUnselectColor;
    self.sbNum.textColor = dateUnselectColor;
    
    self.pnInd.hidden = YES;
    self.vtInd.hidden = YES;
    self.srInd.hidden = YES;
    self.chtInd.hidden = YES;
    self.ptInd.hidden = YES;
    self.sbInd.hidden = YES;
    
    switch (dayOfWeek) {
        case 2:
        {
            self.pn.textColor = selectedColor;
            self.pnNum.textColor = selectedColor;
            self.pnInd.hidden = NO;
            break;
        }
        case 3:
        {
            self.vt.textColor = selectedColor;
            self.vtNum.textColor = selectedColor;
            self.vtInd.hidden = NO;
            break;
        }
        case 4:
        {
            self.sr.textColor = selectedColor;
            self.srNum.textColor = selectedColor;
            self.srInd.hidden = NO;
            break;
        }
        case 5:
        {
            self.cht.textColor = selectedColor;
            self.chtNum.textColor = selectedColor;
            self.chtInd.hidden = NO;
            break;
        }
        case 6:
        {
            self.pt.textColor = selectedColor;
            self.ptNum.textColor = selectedColor;
            self.ptInd.hidden = NO;
            break;
        }
        case 7:
        {
            self.sb.textColor = selectedColor;
            self.sbNum.textColor = selectedColor;
            self.sbInd.hidden = NO;
            break;
        }
        default:
            break;
    }
}

-(NSArray*)getDatesForAllWeek
{
    NSMutableArray *datesMArray = [NSMutableArray new];
    NSDate *today = self.dateToScheduleShow;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [gregorian setLocale:[NSLocale currentLocale]];
    
    NSDateComponents *nowComponents = [gregorian components:NSCalendarUnitYear | NSCalendarUnitWeekOfYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:today];
    
    for (int i =2; i<=7; i++) {
        [nowComponents setWeekday:i]; //Monday
        [nowComponents setWeekOfYear: [nowComponents weekOfYear]]; //this week
        [nowComponents setHour:8]; //8a.m.
        [nowComponents setMinute:0];
        [nowComponents setSecond:0];
        
        NSDate *beginningOfWeek = [gregorian dateFromComponents:nowComponents];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        // extracting components from date
        unsigned units = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay | NSCalendarUnitWeekOfYear;
        NSDateComponents *components = [calendar components:units fromDate:beginningOfWeek];
        
        NSInteger m = [components month];
        NSString *wmounth = [NSString stringWithFormat:@"%li",(long)m];
        if ([wmounth length] == 1) {
            wmounth = [NSString stringWithFormat:@"0%@",wmounth];
        }
        NSInteger d = [components day];
        NSString *wday = [NSString stringWithFormat:@"%li",(long)d];
        if ([wday length] == 1) {
            wday = [NSString stringWithFormat:@"0%@",wday];
        }
        NSInteger y = [components year];
        NSString *wyear = [NSString stringWithFormat:@"%li",(long)y];
        
        NSString *stringDate = [NSString stringWithFormat:@"%@-%@-%@", wyear,wmounth,wday];
        
        [datesMArray addObject:stringDate];
    }
    
    NSArray *dates = [datesMArray copy];
    return dates;
}

+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger indexForArray = self.dayForIntroduse - 2;
    
    if (indexForArray < 0) {
        return 0;
    }
    else {
        
        NSArray *arr = [self.schduleDictionary objectForKey:[self.datesOfWeek objectAtIndex:indexForArray]];
        return [arr count];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = @"Cell2";
    
    NSInteger indexForArray = self.dayForIntroduse - 2;
    
    NSArray *array = [self.schduleDictionary objectForKey:[self.datesOfWeek objectAtIndex:indexForArray]];
    NSArray *arr = [NSArray arrayWithArray:[self sortingOfEventsArray:array]];
    
    ScheduleModel *sm = [[ScheduleModel alloc]init];
    sm = [arr objectAtIndex:[indexPath row]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger weekday = [comps weekday];
    
    NSInteger fromStart = 0;
    NSInteger toEnd = 0;
    //-2!
    if (indexForArray == (weekday - 2)) {
        NSString *begin = [sm.Time objectAtIndex:0];
        NSString *end = [sm.Time objectAtIndex:1];
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"HH:mm"];
        NSString *now = [df stringFromDate:[NSDate date]];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"HH:mm";
        
        NSDate *date = [formatter dateFromString:now];
        NSDate *startDate = [formatter dateFromString:begin];
        NSDate *endDate = [formatter dateFromString:end];
        
        
        if ([ScheduleViewController date:date isBetweenDate:startDate andDate:endDate]) {
            cellID = @"Cell";
            
            int nowM = [[now substringToIndex:2] intValue]*60 +[[now substringFromIndex:3] intValue];
            int startM = [[begin substringToIndex:2] intValue]*60 +[[begin substringFromIndex:3] intValue];
            int endM = [[end substringToIndex:2] intValue]*60 +[[end substringFromIndex:3] intValue];
            
            fromStart = nowM-startM;
            toEnd = endM-nowM;
            

        }
        else {
            cellID = @"Cell2";
        }
    }
    
    if (indexForArray < 0) {
        return nil;
    }
    else {
        
        ScheduleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
        
        cell.subjectName.text = sm.Subject;
        cell.teacher.text =sm.Mentor;
        cell.lessonType.text = sm.Type;
        cell.room.text = [NSString stringWithFormat:@"Аудитроія №%@",sm.Lecture_hall];
        NSArray *times = sm.Time;
        cell.timeToBegin.text = [times objectAtIndex:0];
        cell.paraNum.text = [NSString stringWithFormat:@"%@ пара",sm.Number];
        
        if ([arr count] == indexPath.row+1) {
            cell.perervaViewToAround.hidden = YES;
        }
        else {
            cell.perervaViewToAround.hidden = NO;
            NSString *timeOfPer;
            ScheduleModel *smSecond = [[ScheduleModel alloc]init];
            smSecond = [arr objectAtIndex:[indexPath row]+1];
            NSString *timeEnd1;
            NSString *timeStart2;
            
            if (sm.Time && smSecond.Time) {
                timeEnd1  =[sm.Time objectAtIndex:1];
                timeStart2=[smSecond.Time objectAtIndex:0];
            }
            else {
                timeEnd1 = @"10:10";
                timeStart2 =@"10:25";
            }
            
            int firstMinutes = [[timeEnd1 substringToIndex:2] intValue]*60 +[[timeEnd1 substringFromIndex:3] intValue];
            int secondMinutes = [[timeStart2 substringToIndex:2] intValue]*60 +[[timeStart2 substringFromIndex:3] intValue];
            
            timeOfPer = [NSString stringWithFormat:@"%li",(long)(secondMinutes-firstMinutes)];
            cell.timeOfPerervs.text = [NSString stringWithFormat:@"%@ хв", timeOfPer];
        }
        
        if ([cellID isEqualToString:@"Cell"]) {
            cell.fromStart = fromStart;
            cell.toEnd =toEnd;
            
            [cell.viewCircle setHideCirclePoint:NO];
            //    [self.viewCircle strokeCircleTo:10 total:15 withAnimate:YES];
            NSTimeInterval startDate = [NSDate date].timeIntervalSince1970 - 60*fromStart;//пара почалась 40 хв назад
            NSTimeInterval endDate = [NSDate date].timeIntervalSince1970 + 60*toEnd;//пара закінчиться черз 80 хв
            float lectionPeriod = 60*95;//тривалість пари 120 хв
            if ([[NSDate date] compare:[NSDate dateWithTimeIntervalSince1970:startDate]] == NSOrderedDescending && [[NSDate date] compare:[NSDate dateWithTimeIntervalSince1970:endDate]] == NSOrderedAscending) {//перевірка шо почалась пара, тре запустити анімацію
                
                [cell.viewCircle setDuration:2];
                NSTimeInterval secondsToEnd = [[NSDate date] timeIntervalSince1970]-startDate;
                
                [cell.viewCircle setForegroundStrokeColor:[UIColor colorWithRed:99.0/255.0 green:139.0/255.0 blue:165.0/255.0 alpha:1.0]];
                [cell.viewCircle setBackgroundColor:[UIColor clearColor]];
                [cell.viewCircle strokeCircleTo:(secondsToEnd / lectionPeriod) * 100 total:100 withAnimate:YES];
                [cell.viewCircle setLineWidth:2];
                
            }
            
        }
        return cell;
    }
}

- (void) SwipeRecognizer:(UISwipeGestureRecognizer *)sender
{
    if ( sender.direction == UISwipeGestureRecognizerDirectionLeft )
    {
        NSLog(@" *** SWIPE LEFT ***");
        if (self.dayForIntroduse <7 && self.dayForIntroduse >1) {
            self.dayForIntroduse++;
            [self nowIntroducingToColor:self.dayForIntroduse];
            [self.tableView reloadData];
        }
    }
    
    if ( sender.direction == UISwipeGestureRecognizerDirectionRight ){
        NSLog(@" *** SWIPE RIGHT ***");
        if (self.dayForIntroduse <8 && self.dayForIntroduse >2) {
            self.dayForIntroduse--;
            [self nowIntroducingToColor:self.dayForIntroduse];
            [self.tableView reloadData];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 100;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)sbBtn:(id)sender {
    [self nowIntroducingToColor:7];
    self.dayForIntroduse = 7;
    [self.tableView reloadData];
}

- (IBAction)ptBtn:(id)sender {
    [self nowIntroducingToColor:6];
    self.dayForIntroduse = 6;
    [self.tableView reloadData];
}

- (IBAction)chtBtn:(id)sender {
    [self nowIntroducingToColor:5];
    self.dayForIntroduse = 5;
    [self.tableView reloadData];
}

- (IBAction)srBtn:(id)sender {
    [self nowIntroducingToColor:4];
    self.dayForIntroduse = 4;
    [self.tableView reloadData];
}

- (IBAction)vtBtn:(id)sender {
    [self nowIntroducingToColor:3];
    self.dayForIntroduse = 3;
    [self.tableView reloadData];
}

- (IBAction)pnBtn:(id)sender {
    [self nowIntroducingToColor:2];
    self.dayForIntroduse = 2;
    [self.tableView reloadData];
}
@end
