//
//  KWonliTextCell.m
//  KPI Weeks
//
//  Created by MaximI lnitskiy on 1/29/15.
//  Copyright (c) 2015 Max Ilnitskiy. All rights reserved.
//

#import "KWonliTextCell.h"

@implementation KWonliTextCell

- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)headButton:(id)sender
{
    if (self.urlForAfisha)[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlForAfisha]];
}

@end
