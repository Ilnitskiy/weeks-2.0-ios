//
//  ScheduleTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 13.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICircleAnimationView.h"
@interface ScheduleTableViewCell : UITableViewCell
//@property UICircleAnimationView *circleView1;
@property (weak, nonatomic) IBOutlet UICircleAnimationView *viewCircle;
@property (weak, nonatomic) IBOutlet UILabel *timeOfPerervs;
@property (weak, nonatomic) IBOutlet UILabel *subjectName;
@property (weak, nonatomic) IBOutlet UILabel *room;
@property (weak, nonatomic) IBOutlet UILabel *teacher;
@property (weak, nonatomic) IBOutlet UIView *auditoryIconToAround;
@property (weak, nonatomic) IBOutlet UIView *perervaViewToAround;
@property (weak, nonatomic) IBOutlet UIView *timeViewToAround;
@property (weak, nonatomic) IBOutlet UILabel *timeToBegin;
@property (weak, nonatomic) IBOutlet UILabel *paraNum;

@property (weak, nonatomic) IBOutlet UILabel *lessonType;

@property (assign, nonatomic) NSInteger fromStart;
@property (assign, nonatomic) NSInteger toEnd;

@end
