//
//  ProductManager.m
//  NovaLinia
//
//  Created by Boris Sagan on 1/23/16.
//  Copyright © 2016 Boris Sagan. All rights reserved.
//

#import "NetworkManager.h"
#import "AFNetworking.h"

#import "UniversitetModel.h"
#import "FackultetModel.h"
#import "GroupModel.h"
#import "StudentModel.h"
#import "ScheduleModel.h"

@interface NetworkManager ()

@property (strong, nonatomic) AFHTTPSessionManager *manager;

@end

@implementation NetworkManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

 

#pragma mark - GET Methods

- (void)getUnibersWithComplection:(void(^)(NSArray* univers))completion
                          failure:(void(^)(NSError *error))failure{
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/universities"];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *universities = [(NSDictionary* )responseObject objectForKey:@"Universities"];
                         NSMutableArray *univs = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in universities) {
                             
                             UniversitetModel *univ =
                             [[UniversitetModel alloc] initWithDictionary:dictionary];
                             [univs addObject:univ];
                         }
                         
                         if (completion) {
                             
                             completion([univs copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }

                     }] resume];

    
}

- (void)getFackultetsWithUniverID:(NSString *)idUniver
                       completion:(void(^)(NSArray* facks))completion
                          failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/universities/%@/divisions/",idUniver];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *fackultets = [(NSDictionary* )responseObject objectForKey:@"Divisions"];
                         NSMutableArray *facks = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in fackultets) {
                             
                             FackultetModel *fack =
                             [[FackultetModel alloc] initWithDictionary:dictionary];
                             [facks addObject:fack];
                         }
                         
                         if (completion) {
                             
                             completion([facks copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getComentsWithMentorID:(NSString *)mentorId
                       completion:(void(^)(NSArray* facks))completion
                          failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/mentors/%@/comments/",mentorId];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *fackultets = [(NSDictionary* )responseObject objectForKey:@"Comments"];
                         NSMutableArray *facks = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in fackultets) {
                             
                             [facks addObject:dictionary];
                         }
                         
                         if (completion) {
                             
                             completion([facks copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getGroupsWithFacksID:(NSString *)idFack
                  completion:(void(^)(NSArray* groups))completion
                     failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/divisions/%@/groups",idFack];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *groups = [(NSDictionary* )responseObject objectForKey:@"Groups"];
                         NSMutableArray *grps = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in groups) {
                             
                             GroupModel *gp =
                             [[GroupModel alloc] initWithDictionary:dictionary];
                             [grps addObject:gp];
                         }
                         
                         if (completion) {
                             
                             completion([grps copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getStudentsWithGroupID:(NSString *)groupID
                  completion:(void(^)(NSArray* students))completion
                     failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/students/",groupID];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *studnts = [(NSDictionary* )responseObject objectForKey:@"Students"];
                         NSMutableArray *stds = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in studnts) {
                             
                             StudentModel *sm =
                             [[StudentModel alloc] initWithDictionary:dictionary];
                             [stds addObject:sm];
                         }
                         
                         if (completion) {
                             
                             completion([stds copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getEventsWithGroupID:(NSString *)groupID
                    completion:(void(^)(NSArray* evets))completion
                       failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/events/",groupID];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *studnts = [(NSDictionary* )responseObject objectForKey:@"Schedule"];
                         NSMutableArray *stds = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in studnts) {

                             [stds addObject:dictionary];
                         }
                         
                         if (completion) {
                             
                             completion([stds copy]);
                         }
                         //[3]	(null)	@"NSLocalizedDescription" : @"Request failed: unacceptable content-type: text/html"	
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getEventsWithStudentID:(NSString *)studentID
                  completion:(void(^)(NSArray* evets))completion
                     failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/students/%@/events/",studentID];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSArray *studnts = [(NSDictionary* )responseObject objectForKey:@"Schedule"];
                         NSMutableArray *stds = [NSMutableArray array];
                         
                         for (NSDictionary *dictionary in studnts) {

                             [stds addObject:dictionary];
                         }
                         
                         if (completion) {
                             
                             completion([stds copy]);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

- (void)getUniverStuffWithUniverID:(NSString *)idUniver
                       completion:(void(^)(NSDictionary* stuf))completion
                          failure:(void(^)(NSError *error))failure {
    
    NSString *urlString = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/universities/%@",idUniver];
    
    NSString *URLString =[urlString stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET"
                                                                                 URLString:URLString
                                                                                parameters:nil
                                                                                     error:nil];
    
    
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[self.manager dataTaskWithRequest:request
                     completionHandler:^(NSURLResponse * _Nonnull response,
                                         id  _Nullable responseObject,
                                         NSError * _Nullable error) {
                         
                         NSLog(@"%@", responseObject);
                         
                         NSDictionary *stuff = (NSDictionary* )responseObject;
                         
                         if (completion) {
                             
                             completion(stuff);
                         }
                         
                         if (error) {
                             failure(error);
                             
                         }
                         
                     }] resume];
    
    
    
}

#pragma mark - POST Methods

- (void)createUniversitetWithName:(NSString *)name
                             city:(NSString *)city
                   withCompletion:(void(^)(NSInteger univerID))completion
                          failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =@"http://weeks.kiev.ua:3001/universities";
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 name, @"name",
                                                 city, @"city"
                                                 , nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSInteger idUniver = [[idDic objectForKey:@"UniversityID"] intValue];
        if (completion) {
            
            completion(idUniver);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)createFavultetWithUniverID:(NSString *)univerID
                           andName:(NSString *)name
                    withCompletion:(void(^)(NSInteger fackID))completion
                           failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/universities/%@/divisions",univerID];
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 //univerID, @"univerID",
                                                 name, @"name"
                                                 , nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSInteger faculetetID = [[idDic objectForKey:@"DivisionID"] intValue];
        
        if (completion) {
            
            completion(faculetetID);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)createGroupWithFackID:(NSString *)fackID
                      andName:(NSString *)name
                    andCourse:(NSString *)course
               withCompletion:(void(^)(NSInteger groupID))completion
                      failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/divisions/%@/groups/",fackID];
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 course, @"course",
                                                 name, @"name"
                                                 , nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSInteger grpID = [[idDic objectForKey:@"GroupID"] intValue];
        
        if (completion) {
            
            completion(grpID);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)rateMentorWithID:(NSString *)mentorID
                 andRate:(NSString *)rate
            andStudentID:(NSString *)studentID
          withCompletion:(void(^)(NSString* rated))completion
                 failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/mentor_votes?mentorID=%@&rate=%@&student_id=%@",mentorID,rate,studentID];
    NSString *url = @"http://weeks.kiev.ua:3001/mentor_votes";
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 mentorID, @"mentor_id",
                                                 rate, @"rate",
                                                 studentID, @"student_id"
                                                 , nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSString * rating = [idDic objectForKey:@"Rating"];
        NSLog(@"%@",responseObject);
        if (completion) {
            completion (rating);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)commentMentorWithID:(NSString *)mentorID
                 andText:(NSString *)text
          withCompletion:(void(^)(NSString* rated))completion
                 failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/mentor_votes?mentorID=%@&rate=%@&student_id=%@",mentorID,rate,studentID];
    NSString *url = [NSString stringWithFormat:@"http://weeks.kiev.ua:3001/mentors/%@/comments",mentorID];
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 text, @"text"
                                                 , nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSString * rating = [idDic objectForKey:@"text"];
        
        if (completion) {
            completion (rating);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)createNewStudentWithGroupID:(NSString *)groupID
                            andName:(NSString *)name
                         andProfile:(NSString *)profile
                    andProfile_type:(NSString *)profile_type
                                sex:(NSString *)sex
                              token:(NSString *)token
                         token_type:(NSString *)token_type
                              photo:(NSString *)photo
                     withCompletion:(void(^)(NSInteger studentID))completion
                            failure:(void(^)(NSError *error))failure {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    
//    NSString *phtUrl =[photo stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *tokenEncd =[token stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *nameNckd =[name stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
    
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/students/",groupID];
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 name, @"name",
                                                 profile, @"profile",
                                                 profile_type, @"profile_type",
                                                 sex, @"sex",
                                                 token, @"token",
                                                 token_type, @"token_type",
                                                 photo, @"photo",
                                                 nil];
    
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        NSDictionary *idDic = (NSDictionary* )responseObject;

        NSInteger grpID = [[idDic objectForKey:@"StudentID"] intValue];
        
        if (completion) {
            
            completion(grpID);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        NSLog(@"error: %@", error);
        NSLog(@"Error: %@", [error debugDescription]);
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
}

- (void)createNewEventWithGroupID:(NSString *)groupID
                             date:(NSString *)date
                              day:(NSString *)day
                         interval:(NSString *)interval
                     lecture_hall:(NSString *)lecture_hall
                           mentor:(NSString *)mentor
                           number:(NSString *)number
                             rule:(NSString *)rule
                          subject:(NSString *)subject
                     subject_type:(NSString *)subject_type
                            until:(NSString *)until
                           author:(NSString *)author
                   withCompletion:(void(^)(NSInteger eventID))completion
                          failure:(void(^)(NSError *error))failure {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/events/",groupID];
    //http://weeks.kiev.ua:3001/groups/1/events/?subject=%22test2%22&subject_type=%22lab%22&mentor=%22mentor2%22&lecture_hall=%221233%22&number=1&day=%221,2,3%22&rule=weekly&date=2016-07-13&until=2016-08-13&interval=1&author=1
    date = [date stringByReplacingOccurrencesOfString:@"." withString:@"-"];
    until = [until stringByReplacingOccurrencesOfString:@"." withString:@"-"];
    
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/events/?subject=%@&subject_type=%@&mentor=%@&lecture_hall=%@&number=%@&day=%@&rule=%@&date=%@&until=%@&interval=%@&author=%@",groupID,subject,subject_type,mentor,lecture_hall,number,day,rule,date,until,interval,author];
    NSString *URLString =[url stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLQueryAllowedCharacterSet]];
    
//    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                                 date, @"date",
//                                                 day, @"day",
//                                                 interval, @"interval",
//                                                 lecture_hall, @"lecture_hall",
//                                                 mentor, @"mentor",
//                                                 number, @"number",
//                                                 rule, @"rule",
//                                                 subject, @"subject",
//                                                 subject_type, @"subject_type",
//                                                 until, @"until",
//                                                 nil];
    
    [manager POST:URLString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        NSDictionary *idDic = (NSDictionary* )responseObject;
        
        NSInteger grpID = [[idDic objectForKey:@"EventID"] intValue];
        
        if (completion) {
            
            completion(grpID);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}//url	__NSCFString *	@"http://weeks.kiev.ua:3001/groups/281/events/?subject=Йцук&subject_type=Лекція&mentor=Default Mentor&lecture_hall=13&number=1&day=4&rule=weekly&date=2017-01-12&until=2017-03-12&interval=1&author=1"	0x147a7610

- (void)addEventToStudentWithStudentID:(NSString *)studentID
                                events:(NSArray *)events
                        withCompletion:(void(^)(NSInteger added))completion
                               failure:(void(^)(NSError *error))failure {
    
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/students/%@/add_events/",studentID];;
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 events, @"events",
                                                 nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSInteger idUniver = [[idDic objectForKey:@"Added"] intValue];
        if (completion) {
            
            completion(idUniver);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)removeEventsToStudentWithStudentID:(NSString *)studentID
                                events:(NSArray *)events
                        withCompletion:(void(^)(NSInteger removed))completion
                               failure:(void(^)(NSError *error))failure {
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/students/%@/remove_events/",studentID];;
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 events, @"events",
                                                 nil];
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        NSDictionary *idDic = (NSDictionary* )responseObject;
        NSInteger idUniver = [[idDic objectForKey:@"Removed"] intValue];
        if (completion) {
            
            completion(idUniver);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        
    }];
}

- (void)creatTimeTableWithGroupID:(NSString *)groupID
                         end_time:(NSArray *)times
                   withCompletion:(void(^)(BOOL added))completion
                          failure:(void(^)(NSError *error))failure {
    
//    NSArray *arr = [NSArray arrayWithArray:times];
//    NSDictionary *dic = [arr objectAtIndex:0];
//    NSString *begin = [dic objectForKey:@"start_time"];
//    NSString *end = [dic objectForKey:@"end_time"];
//    NSInteger num = [[dic objectForKey:@"number"]integerValue];
//    NSString * n = [NSString stringWithFormat:@"%li",(long)num];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/groups/%@/timetables",groupID];
    
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                  times, @"TimeTables",
                                                 nil];
    
    
    
    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        NSLog(@"Error: %@", [error debugDescription]);
        NSLog(@"Error: %@", [error localizedDescription]);
    }];//[0]	(null)	@"NSDebugDescription" : @"JSON text did not start with array or object and option to allow fragments not set."
}


#pragma mark - PUT Methods

- (void)putMentorInfoWithMID:(NSString *)mentorId
                          fb:(NSString *)fb
                          vk:(NSString *)vk
                        rank:(NSString *)rank
                       phone:(NSString *)phone
                       email:(NSString *)email
              withCompletion:(void(^)(NSInteger studentID))completion
                     failure:(void(^)(NSError *error))failure {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *url =[NSString stringWithFormat:@"http://weeks.kiev.ua:3001/mentors/%@",mentorId];
    NSMutableDictionary *parametersDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                 email, @"email",
                                                 fb, @"fb",
//                                                 @"NEWname", @"name",
                                                 phone, @"phone",
//                                                 @"NEWphoto", @"photo",
                                                 rank, @"rank",
                                                 vk, @"vk",
                                                 nil];
    
    [manager PUT:url parameters:parametersDictionary success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
//    [manager POST:url parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        
//        NSLog(@"%@",responseObject);
//        NSDictionary *idDic = (NSDictionary* )responseObject;
//        
//        NSInteger grpID = [[idDic objectForKey:@"StudentID"] intValue];
//        
//        if (completion) {
//            
//            completion(grpID);
//        }
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"error: %@", error);
//        NSLog(@"error: %@", error);
//        NSLog(@"Error: %@", [error debugDescription]);
//        NSLog(@"Error: %@", [error localizedDescription]);
//    }];
}

@end
//
