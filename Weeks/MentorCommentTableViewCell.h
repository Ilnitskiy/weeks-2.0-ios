//
//  MentorCommentTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 30.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MentorCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *univerKursLabel;
@property (weak, nonatomic) IBOutlet UILabel *comentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;

@end
