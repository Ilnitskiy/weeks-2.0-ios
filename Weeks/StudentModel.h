//
//  StudentModel.h
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudentModel : NSObject
@property (strong, nonatomic) NSString *birthday;
@property (assign, nonatomic) NSInteger ID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *profile;
@property (strong, nonatomic) NSString *profile_type;
@property (strong, nonatomic) NSString *sex;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *token_type;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
