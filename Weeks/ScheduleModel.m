//
//  ScheduleModel.m
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "ScheduleModel.h"

@implementation ScheduleModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        _Id = [dictionary objectForKey:@"Id"];
        _Subject = [dictionary objectForKey:@"Subject"];
        _Type = [dictionary objectForKey:@"Type"];
        _Mentor = [dictionary objectForKey:@"Mentor"];
        _Lecture_hall = [dictionary objectForKey:@"Lecture hall"];
        _Latitude = [dictionary objectForKey:@"Latitude"];
        _Number = [dictionary objectForKey:@"Number"];
        _Time = [dictionary objectForKey:@"Time"];
        _Dates = [dictionary objectForKey:@"Dates"];
    }
    
    return self;
}
@end
