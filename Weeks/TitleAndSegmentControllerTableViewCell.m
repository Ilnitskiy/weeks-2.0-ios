//
//  TitleAndSegmentControllerTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 29.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "TitleAndSegmentControllerTableViewCell.h"

@implementation TitleAndSegmentControllerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.segmentChoseWeek.selectedSegmentIndex = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
