//
//  ParaBeginEndTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 26.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParaBeginEndTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *paraNumLabel;
@property (weak, nonatomic) IBOutlet UIButton *beginBtnOL;
@property (weak, nonatomic) IBOutlet UIButton *endBtnOL;

- (IBAction)beginBtnAction:(id)sender;
- (IBAction)endBtnAction:(id)sender;

@end
