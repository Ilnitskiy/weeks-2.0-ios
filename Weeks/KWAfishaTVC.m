//
//  KWAfishaTVC.m
//  KPI Weeks
//
//  Created by MaximI lnitskiy on 1/29/15.
//  Copyright (c) 2015 Max Ilnitskiy. All rights reserved.
//

#import "KWAfishaTVC.h"
#import "KWtextAndPhotoCell.h"
#import "KWonliPhotoCell.h"
#import "KWonliTextCell.h"
#import "AFNetworking.h"
#import "KWDeteilAfishaViewController.h"
#import "KWTextPostAfishaVC.h"
#import "TutorialUniverChooseViewController.h"

#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad

#define SERVER_URL @"http://afisha.weeks.kiev.ua/"

@interface KWAfishaTVC () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) AFHTTPSessionManager *manager;

@end

@implementation KWAfishaTVC

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *tutName = @"tutorialAfisha";
    if (![usDef objectForKey:tutName]) {
        TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.tutToDel = tutName;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    if (IDIOM == IPAD)
    {
        NSDictionary *size = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica-Light" size:40.0],UITextAttributeFont,[UIColor whiteColor],UITextAttributeTextColor, nil];
        self.navigationController.navigationBar.titleTextAttributes = size;
    }
    self.afisha = [usDef objectForKey:@"Afisha"];
    
    self.navigationItem.title = @"Афіша";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:85.0/255.0 blue:115.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (IDIOM == IPAD) {
        UINavigationBar *navigationBar = [[self navigationController] navigationBar];
        CGRect frame = [navigationBar frame];
        frame.size.height = 62.0f;
        [navigationBar setFrame:frame];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    if ([self needUpdate]) {
        
        [self updateAfisha];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.afisha count];;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *post = [self.afisha objectAtIndex:[indexPath row]];
    
    if ([[post objectForKey:@"imageAL"] isKindOfClass:[NSData class]]) {
        if (![[post objectForKey:@"text"] isEqualToString:@"null"]) {
            KWtextAndPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textAndPhoto" forIndexPath:indexPath];
            
            cell.name.text = [post objectForKey:@"name"];
            cell.text.text = [post objectForKey:@"text"];
            cell.type.text = [post objectForKey:@"type"];
            [cell.afishaIMG setImage:[UIImage imageWithData:[post objectForKey:@"imageAL"]]];
            [cell.logoIMG setImage:[UIImage imageWithData:[post objectForKey:@"logo"]]];
            cell.urlForAfisha = [post objectForKey:@"url"];
            
            int idAfisha = [[post objectForKey:@"id"]integerValue];
            [self viewAfisha:idAfisha];
            
            //NSString* urlAfisha = [post objectForKey:@"url"];
            if ([[post objectForKey:@"url"] isEqualToString:@"null"]) {
                cell.dots.hidden = YES;
            }else cell.dots.hidden = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
            return cell;
        }
        else
        {
            KWonliPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"photoOnli" forIndexPath:indexPath];
            cell.name.text = [post objectForKey:@"name"];
            cell.type.text = [post objectForKey:@"type"];
            [cell.afishaIMG setImage:[UIImage imageWithData:[post objectForKey:@"imageAL"]]];
            [cell.logoIMG setImage:[UIImage imageWithData:[post objectForKey:@"logo"]]];
            cell.urlForAfisha = [post objectForKey:@"url"];
            
            int idAfisha = [[post objectForKey:@"id"]integerValue];
            [self viewAfisha:idAfisha];
            
            //NSString* urlAfisha = [post objectForKey:@"url"];
            if ([[post objectForKey:@"url"] isEqualToString:@"null"]) {
                cell.dots.hidden = YES;
            }else cell.dots.hidden = NO;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
    else
    {
        KWonliTextCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textOnli" forIndexPath:indexPath];
        cell.name.text = [post objectForKey:@"name"];
        cell.text.text = [post objectForKey:@"text"];
        cell.type.text = [post objectForKey:@"type"];
        [cell.logoIMG setImage:[UIImage imageWithData:[post objectForKey:@"logo"]]];
        cell.urlForAfisha = [post objectForKey:@"url"];
        //NSString* urlAfisha = [post objectForKey:@"url"];
        
        int idAfisha = [[post objectForKey:@"id"]integerValue];
        [self viewAfisha:idAfisha];
        
        if ([[post objectForKey:@"url"] isEqualToString:@"null"]) {
            cell.dots.hidden = YES;
        }
        else cell.dots.hidden = NO;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *post = [self.afisha objectAtIndex:[indexPath row]];
    if (IDIOM == IPAD) {
        if ([[post objectForKey:@"imageAL"] isKindOfClass:[NSData class]]) {
            if (![[post objectForKey:@"text"] isEqualToString:@"null"]) return 518;
            else return 490;
        }
        else return 186;
    }
    else
    {
        if ([[post objectForKey:@"imageAL"] isKindOfClass:[NSData class]]) {
            if (![[post objectForKey:@"text"] isEqualToString:@"null"]) return 425;
            else return 411;
        }
        else return 139;
    }

}

-(void)viewAfisha:(int)afishaId
{
    NSString *urlString = [SERVER_URL stringByAppendingString:@"/posters/view"];
    NSDictionary *parameters = @{@"id": [NSString stringWithFormat:@"%i", afishaId]};
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =urlString;
    
    
    
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        NSLog(@"Error: %@", [error debugDescription]);
        NSLog(@"Error: %@", [error localizedDescription]);
    }];//[0]	(null)	@"NSDebugDescription" : @"JSON text did not start with array or object and option to allow fragments not set."


    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameters = @{@"id": [NSString stringWithFormat:@"%i", afishaId]};
//    [manager POST:[SERVER_URL stringByAppendingString:@"/posters/view"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
}

-(void)clickAfisha:(int)afishaId
{
    NSString *urlString = [SERVER_URL stringByAppendingString:@"/posters/click"];
    NSDictionary *parameters = @{@"id": [NSString stringWithFormat:@"%i", afishaId]};
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *url =urlString;
    
    
    
    [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"success!");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        NSLog(@"Error: %@", [error debugDescription]);
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameters = @{@"id": [NSString stringWithFormat:@"%i", afishaId]};
//    [manager POST:[SERVER_URL stringByAppendingString:@"/posters/click"] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"photoOnli"])
    {
        UITableViewCell *cell = (UITableViewCell*)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSDictionary *post = [self.afisha objectAtIndex:[indexPath row]];
        KWDeteilAfishaViewController *DAVC = [segue destinationViewController];
        NSData *dataIMG = [post objectForKey:@"image"];
        
        DAVC.imgForView =dataIMG;
        
        int idAfisha = [[post objectForKey:@"id"]integerValue];
        [self clickAfisha:idAfisha];
    }
    if ([[segue identifier] isEqualToString:@"photoAndText"])
    {
        UITableViewCell *cell = (UITableViewCell*)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSDictionary *post = [self.afisha objectAtIndex:[indexPath row]];
        KWDeteilAfishaViewController *DAVC = [segue destinationViewController];
        NSData *dataIMG = [post objectForKey:@"image"];
        DAVC.imgForView =dataIMG;
        
        int idAfisha = [[post objectForKey:@"id"]integerValue];
        [self clickAfisha:idAfisha];
    }
    if ([[segue identifier] isEqualToString:@"post"])
    {
        UITableViewCell *cell = (UITableViewCell*)sender;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSDictionary *post = [self.afisha objectAtIndex:[indexPath row]];
        // Get reference to the destination view controller
        KWTextPostAfishaVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"Post"];        
        vc.name.text = [post objectForKey:@"name"];
        vc.text.text = [post objectForKey:@"text"];
        vc.type.text = [post objectForKey:@"type"];
        [vc.logoIMG setImage:[UIImage imageWithData:[post objectForKey:@"logo"]]];
        vc.urlForAfisha = [post objectForKey:@"url"];
        //NSString* urlAfisha = [post objectForKey:@"url"];
        
        int idAfisha = [[post objectForKey:@"id"]integerValue];
        [self clickAfisha:idAfisha];
        
        if ([[post objectForKey:@"url"] isEqualToString:@"null"]) {
            vc.dots.hidden = YES;
        }
        else vc.dots.hidden = NO;

        
    }
}

-(BOOL)needUpdate
{
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    
    NSData *lastUpdateData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://77.47.193.171/posters/updated"]];
    
    NSError *error = nil;
    NSMutableDictionary *lastupdt = [NSJSONSerialization
                                     JSONObjectWithData:lastUpdateData
                                     options:NSJSONReadingMutableContainers
                                     error:&error];
    
    NSString *lastUpdate = [lastupdt objectForKey:@"last_updated"];
    
    NSLog(@"%@",lastUpdate);
    
    if ([[userDef objectForKey:@"dateOfLastAfishaUpdate"] isEqualToString:lastUpdate]) {
        return NO;
    }
    else{
        [userDef setObject:lastUpdate forKey:@"dateOfLastAfishaUpdate"];
        return YES;
        
    }
}
-(void)updateAfisha
{
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
    [userDef setObject:nil forKey:@"Afisha"];
    [userDef synchronize];
    
    NSData *afishaData = [NSData dataWithContentsOfURL:[NSURL URLWithString: @"http://77.47.193.171/posters/list/"]];
    
    NSError *error = nil;
    NSArray *JSONDataInTheDictionary = [NSJSONSerialization
                                        JSONObjectWithData:afishaData
                                        options:NSJSONReadingMutableContainers
                                        error:&error];
    NSString * idZ;
    NSString * titleZ;
    NSString * imageURL;
    NSString * urlZ;
    NSString * logoZ;
    NSString * typeZ;
    NSString * text;
    NSString * dateCrt;
    NSData * dataLowQualiti= [[NSData alloc]init];
    NSData * dataHeighQualiti=[[NSData alloc]init];;
    NSData * dataLogo=[[NSData alloc]init];;
    
    NSMutableArray *afisha = [NSMutableArray new];
    NSArray * afishaArray;
    
    if( error )
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        for ( NSDictionary *post in JSONDataInTheDictionary )
        {
            
            idZ = post[@"id"];
            titleZ = post[@"i_title"];
            typeZ= post[@"i_type"];
            urlZ = post[@"link"];
            
            if ([urlZ isKindOfClass:[NSNull class]])
                urlZ = @"null";
            
            imageURL = post[@"image"];
            if ([imageURL isKindOfClass:[NSNull class]])
            {
                dataLowQualiti = nil;
                dataHeighQualiti = nil;
            }
            else
            {
                NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                UIImage *image = [UIImage imageWithData:dataImage];
                
                dataLowQualiti = UIImageJPEGRepresentation(image, 0.9);
                dataHeighQualiti = UIImageJPEGRepresentation(image, 1);
            }
            
            logoZ = post[@"i_logo"];
            
            NSData *dataImage = [NSData dataWithContentsOfURL:[NSURL URLWithString:logoZ]];
            UIImage *image = [UIImage imageWithData:dataImage];
            
            dataLogo = UIImageJPEGRepresentation(image, 1);
            
            text = post[@"text"];
            if ([text isKindOfClass:[NSNull class]])
                text = @"null";
            dateCrt = post[@"created_at"];
            
            NSMutableDictionary*myPost=[NSMutableDictionary new];
            
            [myPost setObject:idZ forKey:@"id"];
            [myPost setObject:titleZ forKey:@"name"];
            [myPost setObject:urlZ forKey:@"url"];
            [myPost setObject:typeZ forKey:@"type"];
            [myPost setObject:text forKey:@"text"];
            [myPost setObject:dateCrt forKey:@"dateCreate"];
            [myPost setObject:dataLogo forKey:@"logo"];
            if(dataLowQualiti) [myPost setObject:dataLowQualiti forKey:@"imageAL"];
            else [myPost setObject:@"null" forKey:@"imageAL"];
            if(dataHeighQualiti)  [myPost setObject:dataHeighQualiti forKey:@"image"];
            else [myPost setObject:@"null" forKey:@"image"];
            
            if ([[userDef objectForKey:@"dateOfLastAfishaUpdate"]isEqualToString:@"2015-02-04 20:35:44"]) {
                [myPost setObject:@"null" forKey:@"imageAL"];
                [myPost setObject:@"null" forKey:@"image"];
                [myPost removeObjectForKey:@"logo"];
            }
            
            [afisha addObject:myPost];
        }
        
        afishaArray = [afisha copy];
        [userDef setObject:afishaArray forKey:@"Afisha"];
        [self.tableView reloadData];
        [indicator stopAnimating];
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
