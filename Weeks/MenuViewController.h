//
//  MenuViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 30.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <RESideMenu/RESideMenu.h>

@interface MenuViewController : RESideMenu <RESideMenuDelegate>

@end
