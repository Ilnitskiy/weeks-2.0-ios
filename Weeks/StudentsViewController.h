//
//  StudentsViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 04.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentsViewController : UIViewController

@property (strong, nonatomic) NSString *groupID;

@end
