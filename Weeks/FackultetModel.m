//
//  FackultetModel.m
//  Weeks
//
//  Created by Max Ilnitskiy on 24.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "FackultetModel.h"

@implementation FackultetModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        _ID = [[dictionary objectForKey:@"id"] intValue];
        _name = [dictionary objectForKey:@"name"];
    }
    
    return self;
}
@end
