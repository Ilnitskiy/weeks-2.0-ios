//
//  AddUniversitetViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 05.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "AddUniversitetViewController.h"
#import "NetworkManager.h"
#import "TextFieldAndCheckIconTableViewCell.h"
#import "CreateGroupViewController.h"

@interface AddUniversitetViewController () <UITableViewDataSource,UITableViewDelegate>
- (IBAction)doneBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *checkPicker;
//@property (strong, nonatomic) NSArray *tableArray;
@property (strong, nonatomic) NSDictionary *tableDictiomary;

@property (strong, nonatomic) NetworkManager *mnetworkManager;

@property (strong, nonatomic) NSString *nameOfUniversitet;
@property (strong, nonatomic) NSString *nameOfCity;
@property (strong, nonatomic) NSString *nameOfFacultet;

@end

@implementation AddUniversitetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.mnetworkManager = [[NetworkManager alloc]init];
    
    NSArray *namesArray = [NSArray arrayWithObjects:@"Назва",@"Місто",@"Факультет", nil];
    NSArray *tfArray = [NSArray arrayWithObjects:@"Наприклад: КНЕУ",@"Наприклад: Київ",@"Наприклад: ФІОТ", nil];
    
    self.tableDictiomary = [NSDictionary dictionaryWithObjects:tfArray forKeys:namesArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableDictiomary count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = @"TextFieldCell";

    TextFieldAndCheckIconTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    switch (indexPath.row) {
        case 0:
        {
            cell.nameOfCellLabel.text = @"Назва";
            cell.inputTextTF.placeholder = @"Наприклад: КНЕУ";
            cell.inputTextTF.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        }
            break;
        case 1:
        {
            cell.nameOfCellLabel.text = @"Місто";
            cell.inputTextTF.placeholder = @"Наприклад: Київ";
            cell.inputTextTF.autocapitalizationType = UITextAutocapitalizationTypeWords;
        }
            break;
        case 2:
        {
            cell.nameOfCellLabel.text = @"Факультет";
            cell.inputTextTF.placeholder = @"Наприклад: ФІОТ";
            cell.inputTextTF.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
        }
            break;
            
        default:
            break;
    }
    
    if ([cell.inputTextTF.text isEqualToString:@""]) {
        cell.checkIcon.hidden =YES;
    }
    else {
        cell.checkIcon.hidden = NO;
    }
    cell.inputTextTF.delegate = self;
    cell.inputTextTF.tag = [indexPath row];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) textFieldDidEndEditing: (UITextField * ) textField {
    switch (textField.tag) {
        case 0:
            self.nameOfUniversitet = textField.text;
            [self.tableView reloadData];
            break;
        case 1:
            self.nameOfCity = textField.text;
            [self.tableView reloadData];
            break;
        case 2:
            self.nameOfFacultet = textField.text;
            [self.tableView reloadData];
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)createFackWithUniverID:(NSInteger) univerID {
    NSString *universitetID = [NSString stringWithFormat:@"%li",(long)univerID];
    
    [self.mnetworkManager createFavultetWithUniverID:universitetID andName:self.nameOfFacultet withCompletion:^(NSInteger fackID) {
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)fackID] forKey:@"Facultet ID"];
    } failure:^(NSError *error) {
        
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)doneBtn:(id)sender {
    
    [self.mnetworkManager createUniversitetWithName:self.nameOfUniversitet city:self.nameOfCity withCompletion:^(NSInteger univerID) {
        
        [self createFackWithUniverID:univerID];
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        [usDef setObject:[NSString stringWithFormat:@"%li",(long)univerID] forKey:@"Univer ID"];
        
    } failure:^(NSError *error) {
        
    }];
    
//    [self.navigationController dismissViewControllerAnimated:YES
//                                           completion:nil];
//    [self.navigationController popToRootViewControllerAnimated:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateGroupViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addGroup"];
    
    [self.navigationController pushViewController:viewController animated:YES];
}
@end
