//
//  KWDeteilAfishaViewController.m
//  KPI Weeks
//
//  Created by Maxim Ilnitskiy on 12/19/14.
//  Copyright (c) 2014 Max Ilnitskiy. All rights reserved.
//

#import "KWDeteilAfishaViewController.h"

@interface KWDeteilAfishaViewController ()

@end

@implementation KWDeteilAfishaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *img = [UIImage imageWithData:self.imgForView];
    self.flayer.image = img;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancle:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
