//
//  LoginViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 04.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "LoginViewController.h"
#import "FBSDKCoreKit.h"
#import "FBSDKLoginKit.h"
#import "VKSdk.h"

@interface LoginViewController () <VKSdkDelegate,VKSdkUIDelegate,VKApiObject>
@property (weak, nonatomic) IBOutlet UIButton *vkLoginBtn;
@property (weak, nonatomic) IBOutlet UIButton *fbLoginBtn;
//@property (strong, nonatomic) IBOutlet FBSDKLoginButton *loginButton;

- (IBAction)fbLoginAction:(id)sender;
- (IBAction)vkLoginAction:(id)sender;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

-(void) vkSdkDidReceiveNewToken:(VKAccessToken*) newToken {
    
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    if (result.token) {
        NSString *usID = result.token.userId;
        VKRequest * audioReq = [[VKApi users] get:@{VK_API_FIELDS : @"sex,photo_50"}];
        
        [audioReq executeWithResultBlock:^(VKResponse * response) {
            
            NSLog(@"Json result: %@", response.json);
            NSDictionary *dict = [(NSArray *) response.json firstObject];
            
            NSString *vkToken = result.token.accessToken;
            NSString *vkName = [NSString stringWithFormat:@"%@ %@", [dict objectForKey:@"first_name"],[dict objectForKey:@"last_name"]];
            NSString *sex;
            if ([[dict objectForKey:@"sex"] integerValue] == 1) {
                sex = @"female";
            }
            else {
                sex = @"male";
            }
            NSString *photoUrl = [dict objectForKey:@"photo_50"];
            
            //NSDictionary *userVKdict = [NSDictionary dictionaryWithObjects:vkName forKeys:@"name" count:1];
            NSDictionary *userVKdict = [NSDictionary dictionaryWithObjectsAndKeys:
                                        vkName, @"name",
                                        usID, @"profile",
                                        @"vk", @"profileType",
                                        sex, @"sex",
                                        vkToken, @"token",
                                        @"ios", @"tokenType",
                                        photoUrl, @"photo",
                                        nil];
            [usDef setObject:userVKdict forKey:@"User INFO"];
            [usDef synchronize];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } errorBlock:^(NSError * error) {
            if (error.code != VK_API_ERROR) {
                [error.vkError.request repeat];
            }
            else {
                NSLog(@"VK error: %@", error);
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Помилка!" message:@"Неможиво виконати запит. Спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
                [alertController addAction:ok];
                
                [self presentViewController:alertController animated:YES completion:nil];
            } 
        }];
        
    } else if (result.error) {
        // Пользователь отменил авторизацию или произошла ошибка
    }
    
}

-(void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    
}

-(void)vkSdkUserAuthorizationFailed {
    
}

-(void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    
}

-(void)viewDidDisappear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


+ (void)getFacebookAccessTokenFromViewController:(UIViewController *)controller completion:(void (^)(BOOL result))completion
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    [login logOut];
    
    [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:controller handler:^(FBSDKLoginManagerLoginResult *loginResult, NSError *error) {
        if (error || loginResult.isCancelled) {
            if (completion)
                completion(NO);
        } else {
            FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
            if (token) {
                if (completion)
                    completion(YES);
            } else {
                if (completion)
                    completion(NO);
            }
        }
    }];
}


- (IBAction)fbLoginAction:(id)sender {
    [LoginViewController getFacebookAccessTokenFromViewController:self completion:^(BOOL result) {
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name,picture,gender"}]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     NSLog(@"fetched user:%@", result);
                     NSDictionary *dic = (NSDictionary *) result;
                     NSString *photoUrl = [[[dic objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
                     
                     NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
                     FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
                     NSString *tokenStr = token.tokenString;
                     NSDictionary *userFBdict = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 [dic objectForKey:@"name"], @"name",
                                                 [dic objectForKey:@"id"], @"profile",
                                                 @"fb", @"profileType",
                                                 [dic objectForKey:@"gender"], @"sex",
                                                 tokenStr, @"token",
                                                 @"ios", @"tokenType",
                                                 photoUrl, @"photo",
                                                 nil];
                     [usDef setObject:userFBdict forKey:@"User INFO"];
                     [usDef synchronize];
                     
                     [self dismissViewControllerAnimated:YES completion:nil];

                 }
             }];
            
        }
    }];
    
}

- (IBAction)vkLoginAction:(id)sender {
    VKSdk* vkinstance = [VKSdk initializeWithAppId:@"5854888"];// //5761074
    [vkinstance registerDelegate:self];
    [vkinstance setUiDelegate:self];
    
    NSArray *SCOPE = @[@"friends", @"email"];
    
    [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
        if (state == VKAuthorizationAuthorized) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else if (error) {
            // Some error happend, but you may try later
        }
    }];
    //[VKSdk forceLogout];
    [VKSdk authorize:SCOPE];
}
@end
