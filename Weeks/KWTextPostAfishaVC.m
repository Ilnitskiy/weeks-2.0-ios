//
//  KWTextPostAfishaVC.m
//  KPI Weeks
//
//  Created by MaximI lnitskiy on 2/3/15.
//  Copyright (c) 2015 Max Ilnitskiy. All rights reserved.
//

#import "KWTextPostAfishaVC.h"

@interface KWTextPostAfishaVC ()

@end

@implementation KWTextPostAfishaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)headButton:(id)sender
{
    if (self.urlForAfisha)[[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlForAfisha]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
