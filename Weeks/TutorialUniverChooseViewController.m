//
//  TutorialUniverChooseViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 27.02.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "TutorialUniverChooseViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TutorialUniverChooseViewController ()

@property (weak, nonatomic) IBOutlet UIButton *undestandBtn;


- (IBAction)undastaded:(id)sender;
@end

@implementation TutorialUniverChooseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.undestandBtn.clipsToBounds = YES;
    self.undestandBtn.layer.cornerRadius = 4.0;
    self.undestandBtn.layer.borderWidth =1.0f;
    self.undestandBtn.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)undastaded:(id)sender {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    if (self.tutToDel) {
        [usDef setBool:YES forKey:self.tutToDel];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
