//
//  GroupModel.h
//  Weeks
//
//  Created by Max Ilnitskiy on 24.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupModel : NSObject
@property (assign, nonatomic) NSInteger ID;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) NSInteger course;
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
