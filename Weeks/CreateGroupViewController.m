//
//  CreateGroupViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 26.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "CreateGroupViewController.h"

#import "SelectTableViewCell.h"
#import "TextFieldAndCheckIconTableViewCell.h"
#import "HighCellWithHeaderTableViewCell.h"
#import "ParaBeginEndTableViewCell.h"
#import "ForPickerSpaceTableViewCell.h"

#import "NetworkManager.h"
#import "CreateParaViewController.h"
#import "FBSDKCoreKit.h"
#import "FBSDKLoginKit.h"
#import "VKSdk.h"
#import "LoginViewController.h"


#define LIGHT_GRAY_COLOR [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0]
#define DARK_GRAY_COLOR [UIColor colorWithRed:31/255.0f green:32/255.0f blue:33/255.0f alpha:1.0]

@interface CreateGroupViewController () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIDatePicker *myDatePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *mySimplePicker;

@property (weak, nonatomic) IBOutlet UIButton *doneDateBtnOL;
@property (weak, nonatomic) IBOutlet UIButton *closeDateBtnOl;
@property (weak, nonatomic) IBOutlet UIView *datePickerAndBtnsView;

@property (strong, nonatomic) NSArray *tableViewArray;
@property (strong, nonatomic) NSMutableDictionary *callsSchedule;
@property (strong, nonatomic) NetworkManager *mnetworkManager;
@property (strong, nonatomic) NSString *kurs;
@property (strong, nonatomic) NSString *beginOfSemestr;
@property (strong, nonatomic) NSString *endOfSemestr;
@property (strong, nonatomic) NSString *keyForLessons;
@property (strong, nonatomic) NSString *groupName;
@property NSInteger semesterBtnIndex;

- (IBAction)createGroupBtn:(id)sender;
- (IBAction)closeDateAction:(id)sender;
- (IBAction)doneDateAction:(id)sender;
@end

@implementation CreateGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePickerAndBtnsView.hidden =YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.mnetworkManager = [[NetworkManager alloc]init];
    self.callsSchedule = [[NSMutableDictionary alloc]init];
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    self.beginOfSemestr = nil;
    self.endOfSemestr = nil;
    if ([usDef objectForKey:@"Kurs"]) {
        self.kurs = [NSString stringWithFormat:@"%@ курс",[usDef objectForKey:@"Kurs"]];
    }
    else self.kurs = nil;
    
    for (int i=1; i<7; i++) {
        NSMutableArray *beginEndArray = [NSMutableArray arrayWithObjects:@"Початок", @"Кінець", nil];
        [self.callsSchedule setObject:beginEndArray forKey:[NSString stringWithFormat:@"%li",(long)i]];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 13;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID;
    switch (indexPath.row) {
        case 0:
        {
            cellID = @"TextFieldCell";
            TextFieldAndCheckIconTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.nameOfCellLabel.text = @"Назва";
            cell.inputTextTF.placeholder = @"Наприклад: ІО-25";
            cell.inputTextTF.delegate = self;
            cell.inputTextTF.tag = [indexPath row];
            if ([cell.inputTextTF.text isEqualToString:@""]) {
                cell.checkIcon.hidden =YES;
            }
            else {
                cell.checkIcon.hidden = NO;
            }
            return cell;
            break;
        }
        case 1: {
            cellID = @"SelectableCell";
            SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.nameOfCellLavel.text = @"Курс";
            if (self.kurs) {
                cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                cell.chooseLabel.text = self.kurs;
            } else {
                cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                cell.chooseLabel.text = @"Оберіть";
            }

            return cell;
            break;
        }
        case 2: {
            cellID = @"HeightCellWithTitle";
            HighCellWithHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.titleLabel.text = @"Розклад дзвінків";
            return cell;
            break;
        }
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8: {
            cellID = @"ParaBeginEndCell";
            ParaBeginEndTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            
            NSInteger numPara = [indexPath row]-2;
            cell.paraNumLabel.text = [NSString stringWithFormat:@"%li пара",(long)numPara];
            
            NSArray *bedata= [self.callsSchedule objectForKey:[NSString stringWithFormat:@"%li",(long)numPara]];
            
            NSString *begin =[bedata objectAtIndex:0];
            NSString *end =[bedata objectAtIndex:1];
            
            [cell.beginBtnOL addTarget:self
                                action:@selector(beginSemestrBtnTap:)
                      forControlEvents:UIControlEventTouchUpInside];
            [cell.endBtnOL addTarget:self
                              action:@selector(endSemestrBtnTap:)
                    forControlEvents:UIControlEventTouchUpInside];
            
            cell.beginBtnOL.tag = numPara*10;
            cell.endBtnOL.tag=numPara*10+1;
            
//            cell.beginBtnOL.titleLabel.text =begin;// [bedata objectAtIndex:0];
//            cell.endBtnOL.titleLabel.text = end; //[bedata objectAtIndex:1];
            
            [cell.beginBtnOL setTitle:begin forState: UIControlStateNormal];
            [cell.endBtnOL setTitle:end forState: UIControlStateNormal];
            //Початок", @"Кінець"
            
            if ([begin isEqualToString:@"Початок"]) {
                [cell.beginBtnOL setTitleColor:LIGHT_GRAY_COLOR forState:UIControlStateNormal];
            } else {
                [cell.beginBtnOL setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
            }
            
            if ([end isEqualToString:@"Кінець"]) {
                [cell.endBtnOL setTitleColor:LIGHT_GRAY_COLOR forState:UIControlStateNormal];
            } else {
                [cell.endBtnOL setTitleColor:DARK_GRAY_COLOR forState:UIControlStateNormal];
            }
            
            return cell;
            break;
        }
        case 9: {
            cellID = @"HeightCellWithTitle";
            HighCellWithHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.titleLabel.text = @"Семестр";
            return cell;
            break;
        }
        case 10: {
            cellID = @"SelectableCell";
            SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.nameOfCellLavel.text = @"Початок семестру";
            if (self.beginOfSemestr) {
                NSString *beginka = self.beginOfSemestr;
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:beginka];
                
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setDateFormat:@"dd.MM.yyyy"];
                NSString *stringDate = [dateFormatter2 stringFromDate:dateFromString];
                NSLog(@"%@", stringDate);
                
                cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                cell.chooseLabel.text = stringDate;
            } else {
                cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                cell.chooseLabel.text = @"Оберіть";
            }
            return cell;
            break;
        }
        case 11: {
            cellID = @"SelectableCell";
            SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            cell.nameOfCellLavel.text = @"Кінець семестру";
            if (self.endOfSemestr) {
                NSString *endka = self.endOfSemestr;
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [[NSDate alloc] init];
                dateFromString = [dateFormatter dateFromString:endka];
                
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setDateFormat:@"dd.MM.yyyy"];
                NSString *stringDate = [dateFormatter2 stringFromDate:dateFromString];
                NSLog(@"%@", stringDate);

                
                cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                cell.chooseLabel.text = stringDate;
            } else {
                cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                cell.chooseLabel.text = @"Оберіть";
            }
            return cell;
            break;
        }
        default:
        {
            cellID = @"216pxCell";
            ForPickerSpaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
            return cell;
            break;
        }
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        UIAlertController* alert = [UIAlertController
                                    alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* button0 = [UIAlertAction
                                  actionWithTitle:@"Відмінити"
                                  style:UIAlertActionStyleCancel
                                  handler:^(UIAlertAction * action)
                                  {
                                      //  UIAlertController will automatically dismiss the view
                                  }];
        
        UIAlertAction* button1 = [UIAlertAction
                                  actionWithTitle:@"1 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"1 курс";
                                      [self.tableView reloadData];
                                  }];
        
        UIAlertAction* button2 = [UIAlertAction
                                  actionWithTitle:@"2 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"2 курс";
                                      [self.tableView reloadData];
                                  }];
        UIAlertAction* button3 = [UIAlertAction
                                  actionWithTitle:@"3 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"3 курс";
                                      [self.tableView reloadData];
                                  }];
        
        UIAlertAction* button4 = [UIAlertAction
                                  actionWithTitle:@"4 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"4 курс";
                                      [self.tableView reloadData];
                                  }];
        UIAlertAction* button5 = [UIAlertAction
                                  actionWithTitle:@"5 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"5 курс";
                                      [self.tableView reloadData];
                                  }];
        
        UIAlertAction* button6 = [UIAlertAction
                                  actionWithTitle:@"6 курс"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      self.kurs = @"6 курс";
                                      [self.tableView reloadData];
                                  }];
        
        [alert addAction:button0];
        [alert addAction:button1];
        [alert addAction:button2];
        [alert addAction:button3];
        [alert addAction:button4];
        [alert addAction:button5];
        [alert addAction:button6];
        [self presentViewController:alert animated:YES completion:nil];
    }
    if (indexPath.row == 10) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:12 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        
        self.datePickerAndBtnsView.hidden = NO;
        self.myDatePicker.datePickerMode = UIDatePickerModeDate;
        
        self.semesterBtnIndex = 1;
    }
    if (indexPath.row==11) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:12 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
        
        self.datePickerAndBtnsView.hidden = NO;
        self.myDatePicker.datePickerMode = UIDatePickerModeDate;
        
        self.semesterBtnIndex = 2;
    }
}

-(void) beginSemestrBtnTap:(id)sender {
    
    [self.view endEditing:YES];
    
    self.datePickerAndBtnsView.hidden =NO;
    self.myDatePicker.datePickerMode = UIDatePickerModeTime;
    self.myDatePicker.minuteInterval = 5;
    self.keyForLessons = [NSString stringWithFormat:@"%li",(long)[sender tag]];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
}

-(void) endSemestrBtnTap:(id)sender {
    [self.view endEditing:YES];
    
    self.datePickerAndBtnsView.hidden =NO;
    self.myDatePicker.datePickerMode = UIDatePickerModeTime;
    self.myDatePicker.minuteInterval = 5;
    self.keyForLessons = [NSString stringWithFormat:@"%li",(long)[sender tag]];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        case 1:
        case 10:
        case 11:
        {
            return 39;
            break;
        }
        case 2:
        case 9:
        {
            return 63;
            break;
        }
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8: {
            return 39;
            break;
        }
        default:
        {
            return 216;
            break;
        }
    }
}

-(void) textFieldDidEndEditing: (UITextField * ) textField {
    switch (textField.tag) {
        case 0:
            self.groupName = textField.text;
            [self.tableView reloadData];
            break;
        default:
            break;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setTimeTableWithGroupID:(NSString *) groupID {
    
    NSMutableArray *timesArray = [[NSMutableArray alloc]init];
    for (int i=1; i<7; i++) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        NSArray *arr = [NSArray arrayWithArray:[self.callsSchedule objectForKey:[NSString stringWithFormat:@"%li",(long)i]]];
        if (![[arr objectAtIndex:0] isEqualToString:@"Початок"] && ![[arr objectAtIndex:1] isEqualToString:@"Кінець"]) {
            NSString *start = [arr objectAtIndex:0];
            NSString *end = [arr objectAtIndex:1];
            
            [dict setObject:end forKey:@"end_time"];
            [dict setObject:[NSString stringWithFormat:@"%li",(long)i] forKey:@"number"];
            [dict setObject:start forKey:@"start_time"];
            [timesArray addObject:[dict copy]];
        }
    }
    
    [self.mnetworkManager creatTimeTableWithGroupID:groupID end_time:[timesArray copy] withCompletion:^(BOOL added) {
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        if ([usDef objectForKey:@"User INFO"]) {
            
            NSDictionary *stdntData = [NSDictionary dictionaryWithDictionary:[usDef objectForKey:@"User INFO"]];
            
            [self.mnetworkManager createNewStudentWithGroupID:groupID andName:[stdntData objectForKey:@"name"] andProfile:[stdntData objectForKey:@"profile"]  andProfile_type:[stdntData objectForKey:@"profileType"] sex:[stdntData objectForKey:@"sex"] token:[stdntData objectForKey:@"token"] token_type:[stdntData objectForKey:@"tokenType"] photo:[stdntData objectForKey:@"photo"] withCompletion:^(NSInteger studentID) {
                
                [usDef setObject:[NSString stringWithFormat:@"%li",(long)studentID] forKey:@"Student ID"];
            } failure:^(NSError *error) {
                
            }];
        }
        else {
            
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logOut];
            [VKSdk forceLogout];
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LoginViewController *vc = [sb instantiateViewControllerWithIdentifier:@"login"];
            
            [self presentViewController:vc animated:YES completion:nil];
            
        }
    } failure:^(NSError *error) {
        
    }];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateParaViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addSubject"];
    
    [self.navigationController pushViewController:viewController animated:YES];

}

- (IBAction)createGroupBtn:(id)sender {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *fackID = [usDef objectForKey:@"Facultet ID"];
    NSString *course = [self.kurs substringToIndex:1];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    
    if (fackID && ![course isEqualToString:@"О"] && self.groupName && self.beginOfSemestr && self.endOfSemestr) {
        [usDef setObject:self.beginOfSemestr forKey:@"Begin of semestr"];
        
        [usDef setObject:self.endOfSemestr forKey:@"End of semestr"];
        [usDef synchronize];
        
        [self.mnetworkManager createGroupWithFackID:fackID andName:self.groupName andCourse:course withCompletion:^(NSInteger groupID) {
            NSString *gID = [NSString stringWithFormat:@"%li",(long)groupID];
            [usDef setObject:self.groupName forKey:@"Group name"];
            [usDef setObject:gID forKey:@"Group ID"];
            [usDef synchronize];
            
            [self setTimeTableWithGroupID:gID];
            [indicator stopAnimating];
        } failure:^(NSError *error) {
            [indicator stopAnimating];
        }];
    }
    else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Поля не заповненні" message:@"Необхідно заповнити усі поля!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}
- (IBAction)closeDateAction:(id)sender {
    self.datePickerAndBtnsView.hidden = YES;
}

- (IBAction)doneDateAction:(id)sender {
    NSDate *date = self.myDatePicker.date;
    
    if (self.myDatePicker.datePickerMode == UIDatePickerModeDate) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *timeFromPiker = [formatter stringFromDate:date];
        if (self.semesterBtnIndex == 1) {
            self.beginOfSemestr = timeFromPiker;
        }
        if (self.semesterBtnIndex == 2) {
            self.endOfSemestr = timeFromPiker;
        }
    }
    else {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        
        NSString *timeFromPiker = [formatter stringFromDate:date];
        //
        
        NSInteger keyInt = [self.keyForLessons integerValue];
        NSString *key;
        
        NSMutableArray *arr;
        if (keyInt % 2 ==0 ) {
            key =[NSString stringWithFormat:@"%li",keyInt/10];
            arr = [NSMutableArray arrayWithArray:[self.callsSchedule objectForKey:key]];
            [arr replaceObjectAtIndex:0 withObject:timeFromPiker];
        }
        else {
            key =[NSString stringWithFormat:@"%li",(keyInt-1)/10];
            arr = [NSMutableArray arrayWithArray:[self.callsSchedule objectForKey:key]];
            [arr replaceObjectAtIndex:1 withObject:timeFromPiker];
        }
        
        
        [self.callsSchedule setObject:arr forKey:key];
        
    }
    
    [self.tableView reloadData];
    self.datePickerAndBtnsView.hidden =YES;
    
}
@end
