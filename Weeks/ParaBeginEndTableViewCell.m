//
//  ParaBeginEndTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 26.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "ParaBeginEndTableViewCell.h"

@implementation ParaBeginEndTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)beginBtnAction:(id)sender {
}

- (IBAction)endBtnAction:(id)sender {
}
@end
