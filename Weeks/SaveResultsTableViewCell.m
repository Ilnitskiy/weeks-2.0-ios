//
//  SaveResultsTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 31.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "SaveResultsTableViewCell.h"

@implementation SaveResultsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
