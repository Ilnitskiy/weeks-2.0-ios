//
//  MentorDetailViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 23.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "MentorDetailViewController.h"
#import "NetworkManager.h"
#import <QuartzCore/QuartzCore.h>
#import "MentorCommentTableViewCell.h"
#import "TextFieldAndCheckIconTableViewCell.h"
#import "SaveResultsTableViewCell.h"

#define LIGHT_GRAY_COLOR [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0]
#define DARK_GRAY_COLOR [UIColor colorWithRed:31/255.0f green:32/255.0f blue:33/255.0f alpha:1.0]

@interface MentorDetailViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (strong, nonatomic) NetworkManager *mnetworkManager;
@property (weak, nonatomic) IBOutlet UILabel *nooneCommnets;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *mentorrate;
@property (weak, nonatomic) IBOutlet UILabel *mentorName;
@property (weak, nonatomic) IBOutlet UIImageView *photomentor;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoAndCommentBtn;
@property (strong, nonatomic) NSString *telephonement;
@property (strong, nonatomic) NSString *vkak;
@property (strong, nonatomic) NSString *fbak;
@property (strong, nonatomic) NSString *emailment;
@property (strong, nonatomic) NSString *rankment;

- (IBAction)takeRate:(id)sender;
- (IBAction)closeBtn:(id)sender;
- (IBAction)infoBtn:(id)sender;
- (IBAction)addComment:(id)sender;

@property (strong, nonatomic) NSArray *comentsArray;
//@property (strong, nonatomic) NSArray *infosArray;
@property (strong, nonatomic) NSDictionary *prepod;

@property BOOL isForComments;
@property BOOL needSave;
@end

@implementation MentorDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.needSave = NO;
    self.telephonement =@"";
    self.rankment = @"";
    self.fbak = @"";
    self.vkak = @"";
    self.emailment = @"";
    
    self.nooneCommnets.hidden = YES;
    [self.infoAndCommentBtn setTitle:@"Інфо" forState:UIControlStateNormal];
    
    self.mnetworkManager = [[NetworkManager alloc]init];
    self.isForComments = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.mentorrate.clipsToBounds = YES;
    self.mentorrate.layer.cornerRadius = 15.0;
    self.mentorrate.layer.borderWidth =2.0f;
    self.mentorrate.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.photomentor.clipsToBounds = YES;
    self.photomentor.layer.cornerRadius = 50.0;
    self.photomentor.layer.borderWidth =2.0f;
    self.photomentor.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.mentorName.text = self.name;
    
    float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
    
    self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
    
    self.photomentor.image = [UIImage imageNamed:@"teacher.png"];
    
    [self loadComments];
    [self loadInfos];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    self.univerID = [usDef objectForKey:@"Univer ID"];
}

-(void)loadComments {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    
    [self.mnetworkManager getComentsWithMentorID:[NSString stringWithFormat:@"%li", (long)self.mentorID] completion:^(NSArray *facks) {
        self.comentsArray = [NSArray arrayWithArray:facks];
        if ([facks count] == 0) {
            if (self.isForComments) {
                self.nooneCommnets.hidden = NO;
            }
            else self.nooneCommnets.hidden = YES;
        }
        else self.nooneCommnets.hidden = YES;
        
        [self.tableView reloadData];
        [indicator stopAnimating];
    } failure:^(NSError *error) {
        [indicator stopAnimating];
    }];
}

-(void)loadInfos {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    
    [self.mnetworkManager getUniverStuffWithUniverID:self.univerID completion:^(NSDictionary *stuf) {
        NSArray *subjArr = [NSArray arrayWithArray:[stuf objectForKey:@"mentors"]];
        [indicator stopAnimating];
        for (NSDictionary *dict in subjArr) {
            
            if ([[dict objectForKey:@"id"] integerValue] == self.mentorID) {
                self.prepod = [NSDictionary dictionaryWithDictionary:dict];
                self.telephonement = [dict objectForKey:@"phone"];
                self.rankment = [dict objectForKey:@"rank"];
                self.fbak = [dict objectForKey:@"fb"];
                self.vkak = [dict objectForKey:@"vk"];
                self.emailment = [dict objectForKey:@"email"];
                NSString *ratingstr = [dict objectForKey:@"rating"];
                if ([ratingstr isKindOfClass:[NSNull class]]) {
                    self.rate = @"0.0";
                } else  self.rate = ratingstr;
                
                float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                
                self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                [self.tableView reloadData];
            }
        }
    } failure:^(NSError *error) {
        [indicator stopAnimating];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isForComments) {
        
        return [self.comentsArray count];
    }
    else {
        
        return 6;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId;
    if ([indexPath row] == 5) {
        cellId = @"SaveCell";
        SaveResultsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (self.needSave) {
            cell.saveLabel.textColor = DARK_GRAY_COLOR;
        }
        else {
            cell.saveLabel.textColor = LIGHT_GRAY_COLOR;
        }
        return cell;
        
    }
    else {
        if (self.isForComments) {
            cellId = @"Cell";
            MentorCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
            
            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[self.comentsArray objectAtIndex:[indexPath row]]];
            cell.comentLabel.text = [dict objectForKey:@"text"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else {
            cellId = @"TextFieldCell";
            TextFieldAndCheckIconTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            switch (indexPath.row) {
                case 0:
                    cell.nameOfCellLabel.text = @"Посада";
                    cell.inputTextTF.keyboardType = UIKeyboardTypeDefault;
                    if (![self.rankment isKindOfClass:[NSNull class]]) {
                        cell.inputTextTF.text = self.rankment;
                    }
                    break;
                case 1:
                    cell.nameOfCellLabel.text = @"Телефон";
                    cell.inputTextTF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                    if (![self.telephonement isKindOfClass:[NSNull class]]) {
                        cell.inputTextTF.text = self.telephonement;
                    }
                    break;
                case 2:
                    cell.nameOfCellLabel.text = @"E-mail";
                    cell.inputTextTF.keyboardType = UIKeyboardTypeEmailAddress;
                    if (![self.emailment isKindOfClass:[NSNull class]]) {
                        cell.inputTextTF.text = self.emailment;
                    }
                    break;
                case 3:
                    cell.nameOfCellLabel.text = @"Facebook";
                    cell.inputTextTF.keyboardType = UIKeyboardTypeWebSearch;
                    if (![self.fbak isKindOfClass:[NSNull class]]) {
                        cell.inputTextTF.text = self.fbak;
                    }
                    break;
                case 4:
                    cell.nameOfCellLabel.text = @"VK";
                    cell.inputTextTF.keyboardType = UIKeyboardTypeWebSearch;
                    if (![self.vkak isKindOfClass:[NSNull class]]) {
                        cell.inputTextTF.text =  self.vkak;
                    }
                    break;
                default:
                    break;
            }
            cell.inputTextTF.delegate = self;
            cell.inputTextTF.tag = [indexPath row];
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == 5 && self.needSave) {
        [self.mnetworkManager putMentorInfoWithMID:[NSString stringWithFormat:@"%li",(long)self.mentorID] fb:self.fbak vk:self.vkak rank:self.rankment phone:self.telephonement email:self.emailment withCompletion:^(NSInteger studentID) {
            [self loadInfos];
            
        } failure:^(NSError *error) {
            
        }];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isForComments) {
        NSDictionary *dict = [NSDictionary dictionaryWithDictionary:[self.comentsArray objectAtIndex:[indexPath row]]];
        NSString *text = [dict objectForKey:@"text"];
        
        return 89 + [self heightForText:text];
    }
    else {
        return 44;
    }
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 900;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, 320, MAX_HEIGHT)];
    textView.text = text;
    textView.font = [UIFont boldSystemFontOfSize:12];
    [textView sizeToFit];
    return textView.frame.size.height;
}

-(void) textFieldDidEndEditing: (UITextField * ) textField {
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    switch (textField.tag) {
        case 0:
            self.rankment = textField.text;
            [self.tableView reloadData];
            break;
        case 1:
            self.telephonement = textField.text;
            [self.tableView reloadData];
            break;
        case 2:
            self.emailment = textField.text;
            [self.tableView reloadData];
            break;
        case 3:
            self.fbak = textField.text;
            [self.tableView reloadData];
            break;
        case 4:
            self.vkak = textField.text;
            [self.tableView reloadData];
            break;
            
        default:
            break;
    }
    self.needSave = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.tableView];
    CGPoint contentOffset = self.tableView.contentOffset;
    
    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    [self.tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    {
        CGPoint buttonPosition = [textField convertPoint:CGPointZero
                                                  toView: self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
        
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    //    switch (textField.tag) {
    //        case 0:
    //            break;
    //        case 1:
    //            break;
    //        case 2:
    //            break;
    //        case 3:
    //            break;
    //        case 4:
    //
    //            self.tableView.contentInset =  UIEdgeInsetsMake(0, 0, 80, 0);
    //
    //            break;
    //
    //        default:
    //            break;
    //    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)takeRate:(id)sender {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *studID = [usDef objectForKey:@"Student ID"];
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* button0 = [UIAlertAction
                              actionWithTitle:@"Відмінити"
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                  //  UIAlertController will automatically dismiss the view
                              }];
    
    UIAlertAction* button1 = [UIAlertAction
                              actionWithTitle:@"Чудово"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self.mnetworkManager rateMentorWithID:[NSString stringWithFormat:@"%li",(long)self.mentorID] andRate:@"5" andStudentID:studID withCompletion:^(NSString* rated) {
                                      if ([rated isKindOfClass:[NSNull class]]) {
                                          self.rate = @"0.0";
                                      } else  self.rate = rated;
                                      
                                      float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                                      
                                      self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                                      [self.tableView reloadData];
                                  } failure:^(NSError *error) {
                                      
                                  }];
                              }];
    
    UIAlertAction* button2 = [UIAlertAction
                              actionWithTitle:@"Добре"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self.mnetworkManager rateMentorWithID:[NSString stringWithFormat:@"%li",(long)self.mentorID] andRate:@"4" andStudentID:studID withCompletion:^(NSString* rated) {
                                      if ([rated isKindOfClass:[NSNull class]]) {
                                          self.rate = @"0.0";
                                      } else  self.rate = rated;
                                      
                                      float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                                      
                                      self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                                      [self.tableView reloadData];
                                  } failure:^(NSError *error) {
                                      
                                  }];
                              }];
    UIAlertAction* button3 = [UIAlertAction
                              actionWithTitle:@"Нормально"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self.mnetworkManager rateMentorWithID:[NSString stringWithFormat:@"%li",(long)self.mentorID] andRate:@"3" andStudentID:studID withCompletion:^(NSString* rated) {
                                      if ([rated isKindOfClass:[NSNull class]]) {
                                          self.rate = @"0.0";
                                      } else  self.rate = rated;
                                      
                                      float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                                      
                                      self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                                      [self.tableView reloadData];
                                  } failure:^(NSError *error) {
                                      
                                  }];
                              }];
    
    UIAlertAction* button4 = [UIAlertAction
                              actionWithTitle:@"Погано"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self.mnetworkManager rateMentorWithID:[NSString stringWithFormat:@"%li",(long)self.mentorID] andRate:@"2" andStudentID:studID withCompletion:^(NSString* rated) {
                                      if ([rated isKindOfClass:[NSNull class]]) {
                                          self.rate = @"0.0";
                                      } else  self.rate = rated;
                                      
                                      float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                                      
                                      self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                                      [self.tableView reloadData];
                                  } failure:^(NSError *error) {
                                      
                                  }];
                              }];
    UIAlertAction* button5 = [UIAlertAction
                              actionWithTitle:@"Жахливо"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [self.mnetworkManager rateMentorWithID:[NSString stringWithFormat:@"%li",(long)self.mentorID] andRate:@"1" andStudentID:studID withCompletion:^(NSString* rated) {
                                      if ([rated isKindOfClass:[NSNull class]]) {
                                          self.rate = @"0.0";
                                      } else  self.rate = rated;
                                      
                                      float rat = [self.rate isKindOfClass:[NSNull class]] ? 0 : [self.rate floatValue];
                                      
                                      self.rateLabel.text = [NSString stringWithFormat:@"%.01f",rat];
                                      [self.tableView reloadData];
                                  } failure:^(NSError *error) {
                                      
                                  }];
                              }];
    
    [alert addAction:button0];
    [alert addAction:button1];
    [alert addAction:button2];
    [alert addAction:button3];
    [alert addAction:button4];
    [alert addAction:button5];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)closeBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)infoBtn:(id)sender {
    if (self.isForComments) {
        
        self.nooneCommnets.hidden = YES;
        self.isForComments =NO;
        [self.infoAndCommentBtn setTitle:@"Відгуки" forState:UIControlStateNormal];
        [self loadInfos];
        [self.tableView reloadData];
    }
    else {
        self.isForComments =YES;
        [self.infoAndCommentBtn setTitle:@"Інфо" forState:UIControlStateNormal];
        [self loadComments];
        [self.tableView reloadData];
    }
}

- (IBAction)addComment:(id)sender {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Відгук"
                                                                              message:@"Напишіть коментар про викладача"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        //textField.placeholder = @"";
        textField.textColor = [UIColor blackColor];
        textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleNone;
        textField.secureTextEntry = NO;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Надіслати" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * mailfield = textfields[0];
        NSLog(@"%@",mailfield.text);
        //NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        NSString *mentorID = [NSString stringWithFormat:@"%li",(long)self.mentorID];
        
        if (mailfield.text) {
            [self.mnetworkManager commentMentorWithID:mentorID andText:mailfield.text withCompletion:^(NSString *rated) {
                [self loadComments];
            } failure:^(NSError *error) {
                
            }];
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Відмінити" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    //    if (self.isForComments) {
    //        self.infoAndCommentBtn.titleLabel.text = @"Інфо";
    //    }
    //    else {
    //        self.infoAndCommentBtn.titleLabel.text = @"Відгуки";
    //    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
