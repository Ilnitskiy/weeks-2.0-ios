//
//  StudentModel.m
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "StudentModel.h"

@implementation StudentModel
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        _ID = [[dictionary objectForKey:@"id"] intValue];
        _name = [dictionary objectForKey:@"name"];
        _birthday = [dictionary objectForKey:@"birthday"];
        _profile = [dictionary objectForKey:@"profile"];
        _profile_type = [dictionary objectForKey:@"profile_type"];
        _sex = [dictionary objectForKey:@"sex"];
        _token = [dictionary objectForKey:@"token"];
        _token_type = [dictionary objectForKey:@"token_type"];
    }
    
    return self;
}
@end
