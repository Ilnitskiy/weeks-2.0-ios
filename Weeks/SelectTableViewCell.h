//
//  SelectTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameOfCellLavel;
@property (weak, nonatomic) IBOutlet UILabel *chooseLabel;

@end
