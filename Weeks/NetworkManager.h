//
//  ProductManager.h
//  NovaLinia
//
//  Created by Boris Sagan on 1/23/16.
//  Copyright © 2016 Boris Sagan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

//----- Request for GETTING -----

- (void)getUnibersWithComplection:(void(^)(NSArray* univers))completion
                          failure:(void(^)(NSError *error))failure;

- (void)getFackultetsWithUniverID:(NSString *)idUniver
                       completion:(void(^)(NSArray* facks))completion
                          failure:(void(^)(NSError *error))failure;

- (void)getGroupsWithFacksID:(NSString *)idFack
                  completion:(void(^)(NSArray* groups))completion
                     failure:(void(^)(NSError *error))failure;

- (void)getStudentsWithGroupID:(NSString *)groupID
                    completion:(void(^)(NSArray* students))completion
                       failure:(void(^)(NSError *error))failure;

- (void)getEventsWithGroupID:(NSString *)groupID
                  completion:(void(^)(NSArray* evets))completion
                     failure:(void(^)(NSError *error))failure;

- (void)getEventsWithStudentID:(NSString *)studentID
                    completion:(void(^)(NSArray* evets))completion
                       failure:(void(^)(NSError *error))failure;

- (void)getUniverStuffWithUniverID:(NSString *)idUniver
                        completion:(void(^)(NSDictionary* stuf))completion
                           failure:(void(^)(NSError *error))failure;

//----- Request for POSTING -----

- (void)createUniversitetWithName:(NSString *)name
                             city:(NSString *)city
                   withCompletion:(void(^)(NSInteger univerID))completion
                          failure:(void(^)(NSError *error))failure;

- (void)createFavultetWithUniverID:(NSString *)univerID
                           andName:(NSString *)name
                    withCompletion:(void(^)(NSInteger fackID))completion
                           failure:(void(^)(NSError *error))failure;

- (void)createGroupWithFackID:(NSString *)fackID
                      andName:(NSString *)name
                    andCourse:(NSString *)course
               withCompletion:(void(^)(NSInteger groupID))completion
                      failure:(void(^)(NSError *error))failure;

- (void)getComentsWithMentorID:(NSString *)mentorId
                    completion:(void(^)(NSArray* facks))completion
                       failure:(void(^)(NSError *error))failure;

- (void)createNewStudentWithGroupID:(NSString *)groupID
                            andName:(NSString *)name
                         andProfile:(NSString *)profile
                    andProfile_type:(NSString *)profile_type
                                sex:(NSString *)sex
                              token:(NSString *)token
                         token_type:(NSString *)token_type
                              photo:(NSString *)photo
                     withCompletion:(void(^)(NSInteger studentID))completion
                            failure:(void(^)(NSError *error))failure;

- (void)createNewEventWithGroupID:(NSString *)groupID
                             date:(NSString *)date
                              day:(NSString *)day
                         interval:(NSString *)interval
                     lecture_hall:(NSString *)lecture_hall
                           mentor:(NSString *)mentor
                           number:(NSString *)number
                             rule:(NSString *)rule
                          subject:(NSString *)subject
                     subject_type:(NSString *)subject_type
                            until:(NSString *)until
                           author:(NSString *)author
                   withCompletion:(void(^)(NSInteger eventID))completion
                          failure:(void(^)(NSError *error))failure;

- (void)addEventToStudentWithStudentID:(NSString *)studentID
                                events:(NSArray *)events
                        withCompletion:(void(^)(NSInteger added))completion
                               failure:(void(^)(NSError *error))failure;

- (void)removeEventsToStudentWithStudentID:(NSString *)studentID
                                    events:(NSArray *)events
                            withCompletion:(void(^)(NSInteger removed))completion
                                   failure:(void(^)(NSError *error))failur;

- (void)rateMentorWithID:(NSString *)mentorID
                 andRate:(NSString *)rate
            andStudentID:(NSString *)studentID
          withCompletion:(void(^)(NSString* rated))completion
                 failure:(void(^)(NSError *error))failure;

- (void)commentMentorWithID:(NSString *)mentorID
                    andText:(NSString *)text
             withCompletion:(void(^)(NSString* rated))completion
                    failure:(void(^)(NSError *error))failure;

- (void)creatTimeTableWithGroupID:(NSString *)groupID
                         end_time:(NSArray *)times
                   withCompletion:(void(^)(BOOL added))completion
                          failure:(void(^)(NSError *error))failure;

//----- Request for PUTING -----

- (void)putMentorInfoWithMID:(NSString *)mentorId
                          fb:(NSString *)fb
                          vk:(NSString *)vk
                        rank:(NSString *)rank
                       phone:(NSString *)phone
                       email:(NSString *)email
              withCompletion:(void(^)(NSInteger studentID))completion
                     failure:(void(^)(NSError *error))failure;
@end
