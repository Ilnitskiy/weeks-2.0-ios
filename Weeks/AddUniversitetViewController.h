//
//  AddUniversitetViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 05.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUniversitetViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@end
