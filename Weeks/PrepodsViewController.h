//
//  PrepodsViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 23.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrepodsViewController : UIViewController

@property (strong, nonatomic) NSString *univerID;

@end
