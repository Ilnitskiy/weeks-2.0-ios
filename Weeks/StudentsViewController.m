//
//  StudentsViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 04.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "StudentsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "NetworkManager.h"
#import "StudentModel.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "CreateParaViewController.h"
#import "VKSdk.h"

@interface StudentsViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)nextBtn:(id)sender;
- (IBAction)podilitysBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBr;
@property (strong, nonatomic) NSDictionary *myTable;
@property (strong, nonatomic) NSArray *myTableectionTitles;
@property (weak, nonatomic) IBOutlet UIButton *podilitysBtn;

@property (strong, nonatomic) NetworkManager *mnetworkManager;
@end

@implementation StudentsViewController
{
    NSArray *studentsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    self.groupID = [usDef objectForKey:@"Group ID"];
    
    self.mnetworkManager = [[NetworkManager alloc]init];
    
    [self.mnetworkManager getStudentsWithGroupID:self.groupID completion:^(NSArray *students) {
        studentsArray = students;
        [self makeStudentsDictionary:students];
        [indicator stopAnimating];
    } failure:^(NSError *error) {
        [indicator stopAnimating];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Помилка!" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];
    
    //    NSDictionary *tempData = @{
    //                                @"А":@[@"Аня",@"Алексей",@"Анастасия"],
    //                                @"Б":@[@"Богдан",@"Борис"],
    //                                @"Д":@[@"Дима",@"Даша"],
    //                                @"С":@[@"Света",@"Слава",@"Саша"],
    //                                @"Т":@[@"Таня",@"Толик"],
    //                                @"Я":@[@"Яна",@"Ярослав"],
    //                                };
    //
    //
    //    self.myTableectionTitles = [[tempData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    //    NSString *str = [self.myTableectionTitles lastObject];
    //    NSMutableArray *lastSection = [[tempData objectForKey:str] mutableCopy];
    //    [lastSection addObject:@"weeksLastCell"];
    //
    //    NSMutableDictionary *dictWithCell = [tempData mutableCopy];
    //
    //    [dictWithCell setObject:lastSection forKey:[self.myTableectionTitles lastObject]];
    //
    //    self.myTable=[dictWithCell copy];
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.podilitysBtn.clipsToBounds = YES;
    self.podilitysBtn.layer.cornerRadius = 2.0;
    self.podilitysBtn.layer.borderWidth =1.0f;
    self.podilitysBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.myTableectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.myTableectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [self.myTable objectForKey:sectionTitle];
    return [sectionAnimals count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    
    //    if ([[sectionUnivers objectAtIndex:indexPath.row] isKindOfClass:[NSString class]]) {
    //        cell.imageView.hidden = YES;
    //        cell.textLabel.text =@"";
    //        //cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    //        return cell;
    //    }
    //    else {
    StudentModel *sm = [sectionUnivers objectAtIndex:indexPath.row];
    NSString *universitet = sm.name;
    
    cell.imageView.hidden = NO;
    cell.textLabel.text = universitet;
    cell.imageView.image = [UIImage imageNamed:@"profileImg"];
    cell.imageView.clipsToBounds = YES;
    cell.imageView.layer.cornerRadius = 9.0;
    //
    //    CGFloat headerViewHeight = [self.tableView headerViewForSection:0].frame.size.height;
    
    return cell;
    //    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    return self.myTableectionTitles;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        return 44;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSArray *arrayToFilter = studentsArray;
    
    if (searchText.length) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@", searchText];
        
        NSArray *filteredArray = [arrayToFilter filteredArrayUsingPredicate:bPredicate];
        
        arrayToFilter = filteredArray;
    }
    
    [self makeStudentsDictionary:arrayToFilter];
}

-(void)makeStudentsDictionary: (NSArray*)beData {
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (StudentModel *student in beData) {
        
        NSString *firstLetter = [student.name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:student];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:student];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable=[abcUnivers copy];
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    //    NSString *str = [self.myTableectionTitles lastObject];
    //    NSMutableArray *lastSection = [[abcUnivers objectForKey:str] mutableCopy];
    //    [lastSection addObject:@"weeksLastCell"];
    //
    //    NSMutableDictionary *dictWithCell = [abcUnivers mutableCopy];
    //
    //    [dictWithCell setObject:lastSection forKey:[self.myTableectionTitles lastObject]];
    //
    //    self.myTable=[dictWithCell copy];
    
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)nextBtn:(id)sender {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *gID = [usDef objectForKey:@"Group ID"];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];

    [self.mnetworkManager getEventsWithGroupID:gID completion:^(NSArray *evets) {
        if ([evets count]>0) {
            [usDef setObject:evets forKey:@"Schedule"];
            [usDef synchronize];
            [indicator stopAnimating];
            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"] animated:NO completion:nil];
        }
        else {
            [indicator stopAnimating];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Розклад відсутній" message:@"Створіть свій розклад в декілька етапів" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Створити" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                CreateParaViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addSubject"];
                
                [self.navigationController pushViewController:viewController animated:YES];
            }];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }
    } failure:^(NSError *error) {
        [indicator stopAnimating];
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Помилка!" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }];

    
    
    
}

- (IBAction)podilitysBtnAct:(id)sender {
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSDictionary *stdnt = [usDef objectForKey:@"User INFO"];
    if (stdnt) {
        NSString *typeSS = [stdnt objectForKey:@"profileType"];
        if ([typeSS isEqualToString:@"fb"]) {
            FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
            [content setContentTitle:@"Weeks"];
            [content setContentDescription:@"Розклад твоїх заннять у тебе в телефоні!"];
            content.contentURL = [NSURL URLWithString:@"https://itunes.apple.com/ua/app/weeks-schedule/id1090144427?mt=8"];
//            [FBSDKShareDialog showFromViewController:self
//                                         withContent:content
//                                            delegate:nil];
            
            FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
            dialog.mode = FBSDKShareDialogModeFeedWeb;
            dialog.shareContent = content;
            dialog.fromViewController = self;
            [dialog show];

        }
        else if ([typeSS isEqualToString:@"vk"]) {
            VKShareDialogController * shareDialog = [VKShareDialogController new];
            shareDialog.text = @"Розклад твоїх заннять у тебе в телефоні!";
            //-115167012_403545112
            shareDialog.vkImages = @[@"-115167012_403545112"];
            shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:@"Weeks" link:[NSURL URLWithString:@"https://itunes.apple.com/ua/app/weeks-schedule/id1090144427?mt=8"]];
            [shareDialog setCompletionHandler:^(VKShareDialogController *dialog, VKShareDialogControllerResult result) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [self presentViewController:shareDialog animated:YES completion:nil];
        }
    }
    }
@end
