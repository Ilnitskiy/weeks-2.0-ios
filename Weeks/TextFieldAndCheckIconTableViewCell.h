//
//  TextFieldAndCheckIconTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldAndCheckIconTableViewCell : UITableViewCell 
@property (weak, nonatomic) IBOutlet UILabel *nameOfCellLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;
@property (weak, nonatomic) IBOutlet UITextField *inputTextTF;
@end
