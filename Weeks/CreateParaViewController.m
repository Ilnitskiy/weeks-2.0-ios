//
//  CreateParaViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 26.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "CreateParaViewController.h"
#import "TextFieldAndCheckIconTableViewCell.h"
#import "SelectTableViewCell.h"
#import "TitleAndSegmentControllerTableViewCell.h"
#import "HighCellWithHeaderTableViewCell.h"
#import "ForPickerSpaceTableViewCell.h"
#import "SubjectsViewController.h"
#import "PrepodsViewController.h"
#import "NetworkManager.h"
#import "TutorialUniverChooseViewController.h"

#define LIGHT_GRAY_COLOR [UIColor colorWithRed:199/255.0f green:199/255.0f blue:205/255.0f alpha:1.0]
#define DARK_GRAY_COLOR [UIColor colorWithRed:31/255.0f green:32/255.0f blue:33/255.0f alpha:1.0]

@interface CreateParaViewController () <UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentToChose;

@property BOOL isEasyType;
@property BOOL isItFirstWeek;
@property (strong, nonatomic) NSArray *tableViewArray;
@property (strong, nonatomic) NSString *subjName;
@property (strong, nonatomic) NSString *auditory;
@property (strong, nonatomic) NSString *mentor;
@property (strong, nonatomic) NSString *type;
@property NSInteger semesterBtnIndex;
@property (strong, nonatomic) NSString *beginPara;
@property (strong, nonatomic) NSString *endPara;
@property (strong, nonatomic) NSString *numOfPara;
@property (strong, nonatomic) NSString *dayOfweek;
@property (strong, nonatomic) NSString *repiteRule;
@property (strong, nonatomic) NSString *interval;

@property (strong, nonatomic) NetworkManager *mnetworkManager;

@property (weak, nonatomic) IBOutlet UIView *viewWithDataPicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)segmentAction:(id)sender;
- (IBAction)doneBtn:(id)sender;
- (IBAction)donePickerBtn:(id)sender;
- (IBAction)canclePicherBtn:(id)sender;

@end

@implementation CreateParaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    [usDef removeObjectForKey:@"subjNameToPredmet"];
    [usDef removeObjectForKey:@"teachernameToPredmet"];
    [usDef removeObjectForKey:@"auditoryNameToPredmet"];
    [usDef synchronize];
    
    NSString *paraNumAC = [usDef objectForKey:@"paraNumAC"];
    NSString *paraDayAC = [usDef objectForKey:@"paraDayAC"];
    
    self.isEasyType = YES;
    self.isItFirstWeek = YES;
    self.segmentToChose.selectedSegmentIndex = 0;
    self.interval = @"1";
    self.mentor = nil;
    self.repiteRule = @"Щотижня";
    self.subjName = nil;
    self.numOfPara = nil;
    self.dayOfweek = nil;
    
    self.datePicker.date = [NSDate date];
    
    self.subjName=nil;
    self.auditory=nil;
    self.type=nil;
    self.beginPara=[usDef objectForKey:@"Begin of semestr"];
    self.endPara=[usDef objectForKey:@"End of semestr"];
    self.numOfPara=nil;
    self.dayOfweek=nil;
    self.mnetworkManager = [[NetworkManager alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *tutName = @"tutorialAddParaToSimple";
    if (![usDef objectForKey:tutName]) {
        TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.tutToDel = tutName;
        [self presentViewController:vc animated:NO completion:nil];
    }

    self.subjName=[usDef objectForKey:@"subjNameToPredmet"];
    self.mentor=[usDef objectForKey:@"teachernameToPredmet"];
    self.auditory=[usDef objectForKey:@"auditoryNameToPredmet"];
    
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isEasyType) return 8;
    else return 13;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID;
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    
    if (self.isEasyType) {
        switch (indexPath.row) {
            case 0:
            {
                
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Назва";
                
                if (self.subjName) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.subjName;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 1: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Тип";
                if (self.type) {
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.type;
                } else
                {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 2: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Викладач";
                if (self.mentor) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.mentor;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 3: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Аудиторія";
                if (self.auditory) {
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.auditory;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 4: {
                cellID = @"SegmentCellWithTitle";
                HighCellWithHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.titleLabel.text = @"Параметри пари";
                return cell;
                break;
            }
            case 5: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Номер пари";
                NSString *acNum = [usDef objectForKey:@"paraNumAC"];
                if (!acNum) {
                    acNum = @"1";
                }
                self.numOfPara = acNum;
                if (self.numOfPara) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.numOfPara;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 6: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"День тижня";
                NSString *acDay = [usDef objectForKey:@"paraDayAC"];
                if (!acDay) {
                    acDay = @"Понеділок";
                }
                self.dayOfweek = acDay;
                if (self.dayOfweek) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.dayOfweek;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            default:
            {
                cellID = @"216pxCell";
                ForPickerSpaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                return cell;
                break;
            }
        }

    }
    else {
        switch (indexPath.row) {
            case 0:
            {
                
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Назва";
                
                if (self.subjName) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.subjName;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 1: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Тип";
                if (self.type) {
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.type;
                } else
                {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 2: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Викладач";
                if (self.mentor) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.mentor;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 3: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Аудиторія";
                if (self.auditory) {
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.auditory;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 4: {
                cellID = @"HeightCellWithTitle";
                HighCellWithHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.titleLabel.text = @"Параметри пари";
                return cell;
                break;
            }
            case 5: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Номер пари";
                NSString *acNum = [usDef objectForKey:@"paraNumAC"];
                if (!acNum) {
                    acNum = @"1";
                }
                self.numOfPara = acNum;
                if (self.numOfPara) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.numOfPara;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 6: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Повторення";
                if (self.repiteRule) {
                    cell.chooseLabel.text = self.repiteRule;
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 7: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"День тижня";
                NSString *acDay = [usDef objectForKey:@"paraDayAC"];
                if (!acDay) {
                    acDay = @"Понеділок";
                }
                self.dayOfweek = acDay;
                if (self.dayOfweek) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.dayOfweek;
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    }
                } else {
                    
                    cell.chooseLabel.text = @"Оберіть";
                    
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                }
                
                
                
                return cell;
                break;
            }
            case 8: {
                cellID = @"HeightCellWithTitle";
                HighCellWithHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.titleLabel.text = @"Параметри повторення";
                return cell;
                break;
            }
            case 9: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Старт";
                
                if ([self.repiteRule isEqualToString:@"Один раз"]) {
                    cell.nameOfCellLavel.text = @"Дата пари";
                }
                else cell.nameOfCellLavel.text = @"Старт";
                
                if (self.beginPara) {
                    NSString *beginka = self.beginPara;
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *dateFromString = [[NSDate alloc] init];
                    dateFromString = [dateFormatter dateFromString:beginka];
                    
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                    [dateFormatter2 setDateFormat:@"dd.MM.yyyy"];
                    NSString *stringDate = [dateFormatter2 stringFromDate:dateFromString];
                    NSLog(@"%@", stringDate);
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = stringDate;
                } else {
                    cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    cell.chooseLabel.text = @"Оберіть";
                }
                return cell;
                break;
            }
            case 10: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Кінець";
                if (self.endPara) {
                    NSString *endka = self.endPara;
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                    NSDate *dateFromString = [[NSDate alloc] init];
                    dateFromString = [dateFormatter dateFromString:endka];
                    
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                    [dateFormatter2 setDateFormat:@"dd.MM.yyyy"];
                    NSString *stringDate = [dateFormatter2 stringFromDate:dateFromString];
                    NSLog(@"%@", stringDate);
                    
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = stringDate;
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    }
                } else {
                    
                    cell.chooseLabel.text = @"Оберіть";
                    
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                }
                return cell;
                break;
            }
            case 11: {
                cellID = @"SelectableCell";
                SelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                cell.nameOfCellLavel.text = @"Інтервал";
                if (self.interval) {
                    cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    cell.chooseLabel.text = self.interval;
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = DARK_GRAY_COLOR;
                    }
                } else {
                    
                    cell.chooseLabel.text = @"Оберіть";
                    
                    if ([self.repiteRule isEqualToString:@"Один раз"]) {
                        cell.nameOfCellLavel.textColor = LIGHT_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                    else
                    {
                        cell.nameOfCellLavel.textColor = DARK_GRAY_COLOR;
                        cell.chooseLabel.textColor = LIGHT_GRAY_COLOR;
                    }
                }
                return cell;
                break;
            }
            default:
            {
                cellID = @"216pxCell";
                ForPickerSpaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
                return cell;
                break;
            }
        }
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    switch (indexPath.row) {
        case 0: {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SubjectsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"subjectsMVC"];
            vc.typeOfData = @"Subjects";
            [self presentViewController:vc animated:YES completion:nil];
            
            break;
        }
        case 1: {
            UIAlertController* alert = [UIAlertController
                                        alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                        message:nil
                                        preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction* button0 = [UIAlertAction
                                      actionWithTitle:@"Відмінити"
                                      style:UIAlertActionStyleCancel
                                      handler:^(UIAlertAction * action)
                                      {
                                          //  UIAlertController will automatically dismiss the view
                                      }];
            
            UIAlertAction* button1 = [UIAlertAction
                                      actionWithTitle:@"Лекція"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          self.type = @"Лекція";
                                          [self.tableView reloadData];
                                      }];
            
            UIAlertAction* button2 = [UIAlertAction
                                      actionWithTitle:@"Практика"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          self.type = @"Практика";
                                          [self.tableView reloadData];
                                      }];
            UIAlertAction* button3 = [UIAlertAction
                                      actionWithTitle:@"Лабораторна"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          self.type = @"Лабораторна";
                                          [self.tableView reloadData];
                                      }];
            
            UIAlertAction* button4 = [UIAlertAction
                                      actionWithTitle:@"Семінар"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          self.type = @"Семінар";
                                          [self.tableView reloadData];
                                      }];
            UIAlertAction* button5 = [UIAlertAction
                                      actionWithTitle:@"Інше"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          self.type = @"Інше";
                                          [self.tableView reloadData];
                                      }];
            
            [alert addAction:button0];
            [alert addAction:button1];
            [alert addAction:button2];
            [alert addAction:button3];
            [alert addAction:button4];
            [alert addAction:button5];
            [self presentViewController:alert animated:YES completion:nil];
            
            break;
        }
            case 2:
        {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PrepodsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"prepodsMVC"];
            
            [self presentViewController:vc animated:YES completion:nil];
            
            break;
        }
            case 3:
        {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SubjectsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"subjectsMVC"];
            vc.typeOfData = @"Classroom";
            [self presentViewController:vc animated:YES completion:nil];
            
            break;
        }
    }

    if (self.isEasyType) {
        switch (indexPath.row) {
            case 5: {
                UIAlertController* alert = [UIAlertController
                                            alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                            message:nil
                                            preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction* button0 = [UIAlertAction
                                          actionWithTitle:@"Відмінити"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction * action)
                                          {
                                              //  UIAlertController will automatically dismiss the view
                                          }];
                
                UIAlertAction* button1 = [UIAlertAction
                                          actionWithTitle:@"1 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"1";
                                              [usDef setObject:@"1" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button2 = [UIAlertAction
                                          actionWithTitle:@"2 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"2";
                                              [usDef setObject:@"2" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button3 = [UIAlertAction
                                          actionWithTitle:@"3 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"3";
                                              [usDef setObject:@"3" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button4 = [UIAlertAction
                                          actionWithTitle:@"4 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"4";
                                              [usDef setObject:@"4" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button5 = [UIAlertAction
                                          actionWithTitle:@"5 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"5";
                                              [usDef setObject:@"5" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button6 = [UIAlertAction
                                          actionWithTitle:@"6 пара"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"6";
                                              [usDef setObject:@"6" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                [alert addAction:button0];
                [alert addAction:button1];
                [alert addAction:button2];
                [alert addAction:button3];
                [alert addAction:button4];
                [alert addAction:button5];
                [alert addAction:button6];
                [self presentViewController:alert animated:YES completion:nil];
                [usDef synchronize];
                break;
            }
            case 6: {
                UIAlertController* alert = [UIAlertController
                                            alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                            message:nil
                                            preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction* button0 = [UIAlertAction
                                          actionWithTitle:@"Відмінити"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction * action)
                                          {
                                              //  UIAlertController will automatically dismiss the view
                                          }];
                
                UIAlertAction* button1 = [UIAlertAction
                                          actionWithTitle:@"Понеділок"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"Понеділок";
                                              [usDef setObject:@"Понеділок" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button2 = [UIAlertAction
                                          actionWithTitle:@"Вівторок"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"Вівторок";
                                              [usDef setObject:@"Вівторок" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button3 = [UIAlertAction
                                          actionWithTitle:@"Середа"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"Середа";
                                              [usDef setObject:@"Середа" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button4 = [UIAlertAction
                                          actionWithTitle:@"Четвер"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"Четвер";
                                              [usDef setObject:@"Четвер" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button5 = [UIAlertAction
                                          actionWithTitle:@"П'ятниця"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"П'ятниця";
                                              [usDef setObject:@"П'ятниця" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button6 = [UIAlertAction
                                          actionWithTitle:@"Субота"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.dayOfweek = @"Субота";
                                              [usDef setObject:@"Субота" forKey:@"paraDayAC"];
                                              [self.tableView reloadData];
                                          }];
                
                [alert addAction:button0];
                [alert addAction:button1];
                [alert addAction:button2];
                [alert addAction:button3];
                [alert addAction:button4];
                [alert addAction:button5];
                [alert addAction:button6];
                [self presentViewController:alert animated:YES completion:nil];
                [usDef synchronize];
                break;
            }
        }
    }
    else {
        switch (indexPath.row) {
            case 5: {
                UIAlertController* alert = [UIAlertController
                                            alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                            message:nil
                                            preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction* button0 = [UIAlertAction
                                          actionWithTitle:@"Відмінити"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction * action)
                                          {
                                              //  UIAlertController will automatically dismiss the view
                                          }];
                
                UIAlertAction* button1 = [UIAlertAction
                                          actionWithTitle:@"1"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"1";
                                              [usDef setObject:@"1" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button2 = [UIAlertAction
                                          actionWithTitle:@"2"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"2";
                                              [usDef setObject:@"2" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button3 = [UIAlertAction
                                          actionWithTitle:@"3"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"3";
                                              [usDef setObject:@"3" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button4 = [UIAlertAction
                                          actionWithTitle:@"4"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"4";
                                              [usDef setObject:@"4" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button5 = [UIAlertAction
                                          actionWithTitle:@"5"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"5";
                                              [usDef setObject:@"5" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                UIAlertAction* button6 = [UIAlertAction
                                          actionWithTitle:@"6"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.numOfPara = @"6";
                                              [usDef setObject:@"6" forKey:@"paraNumAC"];
                                              [self.tableView reloadData];
                                          }];
                
                [alert addAction:button0];
                [alert addAction:button1];
                [alert addAction:button2];
                [alert addAction:button3];
                [alert addAction:button4];
                [alert addAction:button5];
                [alert addAction:button6];
                [self presentViewController:alert animated:YES completion:nil];
                [usDef synchronize];
                break;
            }
            case 6: {
                UIAlertController* alert = [UIAlertController
                                            alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                            message:nil
                                            preferredStyle:UIAlertControllerStyleActionSheet];
                
                UIAlertAction* button0 = [UIAlertAction
                                          actionWithTitle:@"Відмінити"
                                          style:UIAlertActionStyleCancel
                                          handler:^(UIAlertAction * action)
                                          {
                                              //  UIAlertController will automatically dismiss the view
                                          }];
                
                UIAlertAction* button1 = [UIAlertAction
                                          actionWithTitle:@"Щотижня"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.repiteRule = @"Щотижня";
                                              [self.tableView reloadData];
                                          }];
                
                UIAlertAction* button2 = [UIAlertAction
                                          actionWithTitle:@"Один раз"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              self.repiteRule = @"Один раз";
                                              [self.tableView reloadData];
                                          }];
                
                [alert addAction:button0];
                [alert addAction:button1];
                [alert addAction:button2];
                [self presentViewController:alert animated:YES completion:nil];
                
                break;
            }
            case 7: {
                if (![self.repiteRule isEqualToString:@"Один раз"]) {
                    UIAlertController* alert = [UIAlertController
                                                alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                                message:nil
                                                preferredStyle:UIAlertControllerStyleActionSheet];
                    
                    UIAlertAction* button0 = [UIAlertAction
                                              actionWithTitle:@"Відмінити"
                                              style:UIAlertActionStyleCancel
                                              handler:^(UIAlertAction * action)
                                              {
                                                  //  UIAlertController will automatically dismiss the view
                                              }];
                    
                    UIAlertAction* button1 = [UIAlertAction
                                              actionWithTitle:@"Понеділок"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"Понеділок";
                                                  [usDef setObject:@"Понеділок" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    
                    UIAlertAction* button2 = [UIAlertAction
                                              actionWithTitle:@"Вівторок"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"Вівторок";
                                                  [usDef setObject:@"Вівторок" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    UIAlertAction* button3 = [UIAlertAction
                                              actionWithTitle:@"Середа"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"Середа";
                                                  [usDef setObject:@"Середа" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    
                    UIAlertAction* button4 = [UIAlertAction
                                              actionWithTitle:@"Четвер"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"Четвер";
                                                  [usDef setObject:@"Четвер" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    UIAlertAction* button5 = [UIAlertAction
                                              actionWithTitle:@"П'ятниця"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"П'ятниця";
                                                  [usDef setObject:@"П'ятниця" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    UIAlertAction* button6 = [UIAlertAction
                                              actionWithTitle:@"Субота"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.dayOfweek = @"Субота";
                                                  [usDef setObject:@"Субота" forKey:@"paraDayAC"];
                                                  [self.tableView reloadData];
                                              }];
                    
                    [alert addAction:button0];
                    [alert addAction:button1];
                    [alert addAction:button2];
                    [alert addAction:button3];
                    [alert addAction:button4];
                    [alert addAction:button5];
                    [alert addAction:button6];
                    [self presentViewController:alert animated:YES completion:nil];
                    [usDef synchronize];
                }
                
                break;
                    
            }
            case 9:
            {
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:12 inSection:0];
                [self.tableView scrollToRowAtIndexPath:indexPath
                                      atScrollPosition:UITableViewScrollPositionTop
                                              animated:YES];
                
                self.viewWithDataPicker.hidden = NO;
                self.datePicker.datePickerMode = UIDatePickerModeDate;
                
                self.semesterBtnIndex = 1;
                
                break;
            }
            case 10:
            {
                if (![self.repiteRule isEqualToString:@"Один раз"]) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:12 inSection:0];
                    [self.tableView scrollToRowAtIndexPath:indexPath
                                          atScrollPosition:UITableViewScrollPositionTop
                                                  animated:YES];
                    
                    self.viewWithDataPicker.hidden = NO;
                    self.datePicker.datePickerMode = UIDatePickerModeDate;
                    
                    self.semesterBtnIndex = 2;
                }
                break;
            }
                
            case 11: {
                if (![self.repiteRule isEqualToString:@"Один раз"]) {
                    UIAlertController* alert = [UIAlertController
                                                alertControllerWithTitle:nil      //  Must be "nil", otherwise a blank title area will appear above our two buttons
                                                message:nil
                                                preferredStyle:UIAlertControllerStyleActionSheet];
                    
                    UIAlertAction* button0 = [UIAlertAction
                                              actionWithTitle:@"Відмінити"
                                              style:UIAlertActionStyleCancel
                                              handler:^(UIAlertAction * action)
                                              {
                                                  //  UIAlertController will automatically dismiss the view
                                              }];
                    
                    UIAlertAction* button1 = [UIAlertAction
                                              actionWithTitle:@"1"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.interval = @"1";
                                                  [self.tableView reloadData];
                                              }];
                    
                    UIAlertAction* button2 = [UIAlertAction
                                              actionWithTitle:@"2"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.interval = @"2";
                                                  [self.tableView reloadData];
                                              }];
                    UIAlertAction* button3 = [UIAlertAction
                                              actionWithTitle:@"3"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action)
                                              {
                                                  self.interval = @"3";
                                                  [self.tableView reloadData];
                                              }];
                    
                    
                    [alert addAction:button0];
                    [alert addAction:button1];
                    [alert addAction:button2];
                    [alert addAction:button3];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                break;
            }

                
            default:
                break;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.isEasyType) {
        switch (indexPath.row) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
            case 9:
            case 10:
            case 11:
            {
                return 39;
                break;
            }
            case 4:
            case 8:
            {
                return 63;
                break;
            }
            default:
            {
                return 216;
                break;
            }
        }
    }
    else {
        switch (indexPath.row) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            {
                return 39;
                break;
            }
            case 4:{
                return 74;
                break;
            }
            default:
            {
                return 216;
                break;
            }
        }
    }
    
}


-(void) textFieldDidEndEditing: (UITextField * ) textField {
    switch (textField.tag) {
        case 0:
        {
            self.subjName = textField.text;
            [self.tableView reloadData];
            break;
        }
        case 3:
        {
            self.auditory = textField.text;
            [self.tableView reloadData];
        }
        default:
            break;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segmentAction:(id)sender {
    if (self.segmentToChose.selectedSegmentIndex == 1) {
        self.isEasyType = NO;
        
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        NSString *tutName = @"tutorialAddParaToAdditional";
        if (![usDef objectForKey:tutName]) {
            TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            vc.tutToDel = tutName;
            [self presentViewController:vc animated:NO completion:nil];
        }
    }
    else if (self.segmentToChose.selectedSegmentIndex == 0) {
        self.isEasyType = YES;
    }
    [self.tableView reloadData];
}

- (IBAction)doneBtn:(id)sender {
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];

    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *groupId = [usDef objectForKey:@"Group ID"];
    NSString *studentID;if([usDef objectForKey:@"Student ID"]){studentID=[usDef objectForKey:@"Student ID"];}else{studentID =@"1";}
    
    NSString *date;
    NSString *interval=self.interval;
    NSString *lectureHall=self.auditory;
    NSString *mentor=self.mentor;
    NSString *number=self.numOfPara;
    NSString *rule=self.repiteRule;
    NSString *subject=self.subjName;
    NSString *subjectType=self.type;
    NSString *until=self.endPara;
    NSString *author = studentID;
    
    if ([rule isEqualToString:@"Один раз"]) {
        rule = @"singular";
    }
    else {
        rule = @"weekly";
    }
    
    [self weekDay];
    NSString *day=self.dayOfweek;
    
    if (!self.isEasyType) {
        date = self.beginPara;
        
        BOOL forWeeklyGood;
        BOOL forSingleDayGood;
        if (groupId && date && lectureHall && mentor && number && rule && subjectType && subject) {
            if (groupId && date && day && interval && lectureHall && mentor && number && rule && subjectType && subject && until) {
                forWeeklyGood = YES;
            }
            else { forWeeklyGood = NO;}
            forSingleDayGood = YES;
        }
        else {
            forWeeklyGood = NO;
            forSingleDayGood = NO;
        }
        
        if (([self.repiteRule isEqualToString:@"Один раз"] && forSingleDayGood) || (![self.repiteRule isEqualToString:@"Один раз"] && forWeeklyGood)) {
            [self.mnetworkManager createNewEventWithGroupID:groupId date:date day:day interval:interval lecture_hall:lectureHall mentor:mentor number:number rule:rule subject:subject subject_type:subjectType until:until author:author withCompletion:^(NSInteger eventID) {
                [indicator stopAnimating];
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Пара додана" message:@"Продовжити додавання пар, чи перейти до розкладу?" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* add = [UIAlertAction actionWithTitle:@"Додати пару" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [self viewDidLoad];
                    [self.tableView reloadData];
                    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                }];
                UIAlertAction* schedule = [UIAlertAction actionWithTitle:@"Розклад" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    //ScheduleNavigation
                    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
                    NSString *gID = [usDef objectForKey:@"Group ID"];
                    [usDef removeObjectForKey:@"paraNumAC"];
                    [usDef removeObjectForKey:@"paraDayAC"];
                    [self.mnetworkManager getEventsWithGroupID:gID completion:^(NSArray *evets) {
                        if ([evets count]>0) {
                            [usDef setObject:evets forKey:@"Schedule"];
                            [usDef removeObjectForKey:@"ScheduleG"];
                            [usDef removeObjectForKey:@"mentorsOfGroup"];
                            [usDef synchronize];

                            [usDef synchronize];
                            [indicator stopAnimating];
                            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"] animated:NO completion:nil];
                        }
                    } failure:^(NSError *error) {
                        [indicator stopAnimating];
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Помилка!" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }];
                     
                    
                }];
                [alertController addAction:add];
                [alertController addAction:schedule];
                
                [self presentViewController:alertController animated:YES completion:nil];
                
                
            } failure:^(NSError *error) {
                [indicator stopAnimating];
            }];
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Поля не заповненні" message:@"Необхідно заповнити усі поля!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [indicator stopAnimating];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else {

        if (date) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            // this is imporant - we set our input date format to match our input string
            // if format doesn't match you'll get nil from your string, so be careful
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [[NSDate alloc] init];
            // voila!
            dateFromString = [dateFormatter dateFromString:date];
            
            NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
            dayComponent.day = 7;
            
            NSCalendar *theCalendar = [NSCalendar currentCalendar];
            NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
            
            NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
            [dateFormatter2 setDateFormat:@"yyyy-MM-dd"];
            date = [dateFormatter2 stringFromDate:nextDate];
            
        }
        else { if (self.isItFirstWeek) date = @"2017-01-02"; else date = @"2017-01-09";}
        
        if (groupId && date && day && lectureHall && mentor && number && subjectType && subject) {
            until = [usDef objectForKey:@"End of semestr"];
            if (!until) {
                until = @"2020-12-31";
            }
            
            [self.mnetworkManager createNewEventWithGroupID:groupId date:date day:day interval:@"2" lecture_hall:lectureHall mentor:mentor number:number rule:@"weekly" subject:subject subject_type:subjectType until:until author:author withCompletion:^(NSInteger eventID) {
                [indicator stopAnimating];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Пара додана" message:@"Продовжити додавання пар, чи перейти до розкладу?" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* add = [UIAlertAction actionWithTitle:@"Додати пару" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    NSInteger num = [[usDef objectForKey:@"paraNumAC"]integerValue];
                    num +=1;
                    [usDef setObject:[NSString stringWithFormat:@"%li",(long)num] forKey:@"paraNumAC"];
                    [usDef synchronize];
                    [self viewDidLoad];
                    [self.tableView reloadData];
                    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
                }];
                UIAlertAction* schedule = [UIAlertAction actionWithTitle:@"Розклад" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    //ScheduleNavigation
                    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
                    NSString *gID = [usDef objectForKey:@"Group ID"];
                    [usDef removeObjectForKey:@"paraNumAC"];
                    [usDef removeObjectForKey:@"paraDayAC"];
                    [self.mnetworkManager getEventsWithGroupID:gID completion:^(NSArray *evets) {
                        if ([evets count]>0) {
                            [usDef setObject:evets forKey:@"Schedule"];
                            [usDef removeObjectForKey:@"ScheduleG"];
                            [usDef removeObjectForKey:@"mentorsOfGroup"];
                            [usDef synchronize];
                            [indicator stopAnimating];
                            [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"] animated:NO completion:nil];
                        }
                    } failure:^(NSError *error) {
                        [indicator stopAnimating];
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Помилка!" message:@"Перевірте підключення до інтернету, або спробуйте пізніше." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
                        [alertController addAction:ok];
                        
                        [self presentViewController:alertController animated:YES completion:nil];
                    }];
                    
                    
                }];
                [alertController addAction:add];
                [alertController addAction:schedule];
                
                [self presentViewController:alertController animated:YES completion:nil];
            } failure:^(NSError *error) {
                [indicator stopAnimating];
            }];
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Поля не заповненні" message:@"Необхідно заповнити усі поля!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Oк" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            [indicator stopAnimating];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

-(void) weekDay {
    if ([self.dayOfweek isEqualToString:@"Понеділок"]) {
        self.dayOfweek = @"1";
    }
    if ([self.dayOfweek isEqualToString:@"Вівторок"]) {
        self.dayOfweek = @"2";
    }
    if ([self.dayOfweek isEqualToString:@"Середа"]) {
        self.dayOfweek = @"3";
    }
    if ([self.dayOfweek isEqualToString:@"Четвер"]) {
        self.dayOfweek = @"4";
    }
    if ([self.dayOfweek isEqualToString:@"П'ятниця"]) {
        self.dayOfweek = @"5";
    }
    if ([self.dayOfweek isEqualToString:@"Субота"]) {
        self.dayOfweek = @"6";
    }
}

- (IBAction)donePickerBtn:(id)sender {
    NSDate *date = self.datePicker.date;
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString *timeFromPiker = [formatter stringFromDate:date];
        if (self.semesterBtnIndex == 1) {
            self.beginPara = timeFromPiker;
        }
        if (self.semesterBtnIndex == 2) {
            self.endPara = timeFromPiker;
        }
    [self.tableView reloadData];
    self.viewWithDataPicker.hidden =YES;
}
- (IBAction)canclePicherBtn:(id)sender {
    self.viewWithDataPicker.hidden = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)controlChanged:(UIControl *)sender {
    CGPoint senderOriginInTableView = [sender convertPoint:CGPointZero toView:self.tableView];
    
    if (self.isItFirstWeek) {
        self.isItFirstWeek = NO;
    }
    else self.isItFirstWeek = YES;
}
@end
