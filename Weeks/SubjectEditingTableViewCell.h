//
//  SubjectEditingTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 24.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubjectEditingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *subjectName;
@property (weak, nonatomic) IBOutlet UILabel *mentorName;
@property (weak, nonatomic) IBOutlet UILabel *classroomNum;
@property (weak, nonatomic) IBOutlet UILabel *subjectType;
@property (weak, nonatomic) IBOutlet UILabel *weekDay;
@property (weak, nonatomic) IBOutlet UILabel *paraNum;

@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;
@end
