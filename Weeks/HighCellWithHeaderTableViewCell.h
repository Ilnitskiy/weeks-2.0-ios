//
//  HighCellWithHeaderTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 26.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighCellWithHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
