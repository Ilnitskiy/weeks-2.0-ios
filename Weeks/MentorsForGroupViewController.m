//
//  MentorsForGroupViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 23.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "MentorsForGroupViewController.h"
#import "NetworkManager.h"
#import "MentorDetailViewController.h"

@interface MentorsForGroupViewController () <UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBr;
@property (strong, nonatomic) NSDictionary *myTable;
@property (strong, nonatomic) NSArray *myTableectionTitles;

@property (strong, nonatomic) NetworkManager *mnetworkManager;

@property (strong, nonatomic) NSString *univerID;
@end

@implementation MentorsForGroupViewController
{
    NSArray *subjectsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    self.univerID = [usDef objectForKey:@"Univer ID"];
    
    self.mnetworkManager = [[NetworkManager alloc]init];
    
    NSArray *arr = [usDef objectForKey:@"mentorsOfGroup"];
    
    if (!arr) {
        [self.mnetworkManager getUniverStuffWithUniverID:self.univerID completion:^(NSDictionary *stuf) {
            NSArray *subjArr = [NSArray arrayWithArray:[stuf objectForKey:@"mentors"]];
            subjectsArray = subjArr;
            [self makeSubjectsDictionary:subjArr];
            NSMutableArray *marr = [NSMutableArray new];
            for (NSDictionary *dictI in subjArr) {
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:dictI];
                if ([[dict objectForKey:@"phone"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"phone"];
                }if ([[dict objectForKey:@"photo"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"photo"];
                }if ([[dict objectForKey:@"rank"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"rank"];
                }if ([[dict objectForKey:@"rating"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"rating"];
                }if ([[dict objectForKey:@"vk"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"vk"];
                }if ([[dict objectForKey:@"email"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"email"];
                }if ([[dict objectForKey:@"fb"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"fb"];
                }if ([[dict objectForKey:@"name"] isKindOfClass:[NSNull class]]) {
                    [dict setObject:@"" forKey:@"name"];
                }
                [marr addObject:[dict copy]];
            }
            
            [usDef setObject:[marr copy] forKey:@"mentorsOfGroup"];
            [indicator stopAnimating];
        } failure:^(NSError *error) {
            [indicator stopAnimating];
        }];
    }
    else {
        [self makeSubjectsDictionary:arr];
        [indicator stopAnimating];
    }
    
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.navigationItem.title = @"Викладачі";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:85.0/255.0 blue:115.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addPara)];
//    
//    self.navigationItem.rightBarButtonItem = addButton;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.myTableectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.myTableectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [self.myTable objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    NSString *subj = [[sectionUnivers objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    cell.textLabel.text = subj;
    
    return cell;
    
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    return self.myTableectionTitles;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"CheckIcon"];
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    NSDictionary *subj = [sectionUnivers objectAtIndex:indexPath.row];
    NSInteger mentorID = [[subj objectForKey:@"id"] integerValue];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MentorDetailViewController *vc = [sb instantiateViewControllerWithIdentifier:@"mentorDetail"];
    vc.mentorID = mentorID;
    vc.name = [subj objectForKey:@"name"];
    
    if ([[subj objectForKey:@"rating"] isKindOfClass:[NSNull class]]) {
        vc.rate = @"0.0";
    }
    else
    {
        vc.rate = [subj objectForKey:@"rating"];
    }
    
    [self presentViewController:vc animated:YES completion:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSArray *arrayToFilter = subjectsArray;
    
    if (searchText.length) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@", searchText];
        
        NSArray *filteredArray = [arrayToFilter filteredArrayUsingPredicate:bPredicate];
        
        arrayToFilter = filteredArray;
        
    }
    
    [self makeSubjectsDictionary:arrayToFilter];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    return nil;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == [tableView numberOfSections] - 1) {
//        return 44;
//    }
//    return 0.01;
//}

-(void)makeSubjectsDictionary: (NSArray*)beData {
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSArray *events = [NSArray arrayWithArray:[usDef objectForKey:@"Schedule"]];
    
    NSMutableArray *elements = [NSMutableArray array];
    NSMutableArray *ids = [NSMutableArray array];
    for (NSDictionary *dictionary in events) {
        [elements addObject:dictionary];
        [ids addObject:[dictionary objectForKey:@"MentorId"]];
    }
    
    NSArray * a = [NSArray arrayWithArray:ids];
    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    for (NSString * string in a) {
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    
    NSArray *array = [NSArray arrayWithArray:unique];
    
    NSCountedSet *namesSet = [[NSCountedSet alloc] initWithArray:array];
    NSMutableArray *idsArray = [[NSMutableArray alloc] initWithCapacity:[array count]];
    
    [namesSet enumerateObjectsUsingBlock:^(id obj, BOOL *stop){
        if ([namesSet countForObject:obj] == 1) {
            [idsArray addObject:obj];
        }
    }];
    
    NSMutableArray *newBeData = [[NSMutableArray alloc]init];
    for (NSDictionary *dict in beData) {
        for (int i=0; i<[unique count]; i++) {
            NSInteger idINB = [[dict objectForKey:@"id"] integerValue];
            
            NSString *idInBEstr = [NSString stringWithFormat:@"%li",(long)idINB];
            
            if ([idInBEstr isEqualToString:[unique objectAtIndex:i]]) {
                [newBeData addObject:dict];
            }
        }
    }
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *teacher in newBeData) {
        NSString *name = [teacher objectForKey:@"name"];
        NSString *firstLetter = [name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:teacher];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:teacher];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


@end
