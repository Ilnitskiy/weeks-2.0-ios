//
//  TitleAndSegmentControllerTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 29.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleAndSegmentControllerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentChoseWeek;

@end
