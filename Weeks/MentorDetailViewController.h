//
//  MentorDetailViewController.h
//  Weeks
//
//  Created by Max Ilnitskiy on 23.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MentorDetailViewController : UIViewController

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *rate;
@property (assign, nonatomic) NSInteger mentorID;
@property (strong, nonatomic) NSString *univerID;
@end
