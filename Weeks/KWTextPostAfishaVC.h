//
//  KWTextPostAfishaVC.h
//  KPI Weeks
//
//  Created by MaximI lnitskiy on 2/3/15.
//  Copyright (c) 2015 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KWTextPostAfishaVC : UIViewController
@property NSString *urlForAfisha;

@property (weak, nonatomic) IBOutlet UILabel *dots;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UITextView *text;
@property (weak,nonatomic) IBOutlet UIImageView *logoIMG;
- (IBAction)headButton:(id)sender;

@end
