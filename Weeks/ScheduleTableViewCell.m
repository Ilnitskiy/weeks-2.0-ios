//
//  ScheduleTableViewCell.m
//  Weeks
//
//  Created by Max Ilnitskiy on 13.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import "ScheduleTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation ScheduleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.auditoryIconToAround.layer.cornerRadius = 5;
    self.auditoryIconToAround.layer.masksToBounds = YES;
    
    self.perervaViewToAround.layer.cornerRadius = 4;
    self.perervaViewToAround.layer.masksToBounds = YES;
    
    self.timeViewToAround.layer.cornerRadius = 28;
    self.timeViewToAround.layer.masksToBounds = YES;
}

- (void)layoutSubViews
{
    [super layoutSubviews];
    
    [self performSelector:@selector(myMethod) withObject:nil afterDelay:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)myMethod {
    
}

@end
