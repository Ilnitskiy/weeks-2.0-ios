//
//  SubjectsOfGroupViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 16.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "SubjectsOfGroupViewController.h"
#import "CreateParaViewController.h"
#import "ScheduleModel.h"
#import "SubjectEditingTableViewCell.h"
#import "NetworkManager.h"
#import "TutorialUniverChooseViewController.h"

@interface SubjectsOfGroupViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *saveEditingView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBr;
@property (strong, nonatomic) NSDictionary *myTable;
@property (strong, nonatomic) NSArray *myTableectionTitles;

@property (strong, nonatomic) NetworkManager *mnetworkManager;

- (IBAction)saveEditing:(id)sender;

@property BOOL editingOFSubjList;
@end

@implementation SubjectsOfGroupViewController
{
    NSArray *subjectsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *tutName = @"tutorialSubjectsOfGroup";
    if (![usDef objectForKey:tutName]) {
        TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.tutToDel = tutName;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    self.mnetworkManager = [[NetworkManager alloc]init];
    self.editingOFSubjList = NO;
    self.saveEditingView.hidden = YES;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    if (![usDef objectForKey:@"ScheduleG"]) {
        NSArray *events = [NSArray arrayWithArray:[usDef objectForKey:@"Schedule"]];
        [usDef setObject:events forKey:@"ScheduleG"];
    }
    
    NSArray *events = [NSArray arrayWithArray:[usDef objectForKey:@"ScheduleG"]];
    
    NSMutableArray *elements = [NSMutableArray array];
    NSMutableArray *ids = [NSMutableArray array];
    for (NSDictionary *dictionary in events) {
        [elements addObject:dictionary];
        [ids addObject:[dictionary objectForKey:@"Id"]];
    }
    
    if (![usDef objectForKey:@"Student Events IDs"]) {
        [usDef setObject:[ids copy]  forKey:@"Student Events IDs"];
    }
    //NSArray *subjs = [NSArray arrayWithArray:[usDef objectForKey:@"Subjects"]];
    subjectsArray = [elements copy];
    [self makeSubjectsDictionary:elements];
    
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.navigationItem.title = @"Предмети";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:85.0/255.0 blue:115.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addPara)];
    UIBarButtonItem *editSchedule = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"editSchedule"] style:UIBarButtonItemStyleDone target:self action:@selector(editShedule)];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:editSchedule, addButton, nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.myTableectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.myTableectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [self.myTable objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editingOFSubjList) {
        SubjectEditingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EditingCell" forIndexPath:indexPath];
        
        NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
        NSDictionary *subj = [sectionUnivers objectAtIndex:indexPath.row];
        NSString *subName = [subj objectForKey:@"Subject"];
        NSString *type = [subj objectForKey:@"Type"];
        NSString *mentor = [subj objectForKey:@"Mentor"];
        NSString *lecturehall = [subj objectForKey:@"Lecture hall"];
        NSString *number = [subj objectForKey:@"Number"];
        NSArray *arr = [NSArray arrayWithArray:[subj objectForKey:@"Dates"]];
        NSString *dateOfPara;
        if ([arr count]>0) {
            dateOfPara = [self day:[arr objectAtIndex:0]];
        }

        cell.subjectName.text = subName;
        cell.subjectType.text = type;
        cell.mentorName.text = mentor;
        cell.subjectName.text = subName;
        cell.classroomNum.text = lecturehall;
        cell.weekDay.text = dateOfPara;
        cell.paraNum.text = number;
        
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        NSArray *idsForStudent = [NSArray arrayWithArray:[usDef objectForKey:@"Student Events IDs"]];
        cell.checkIcon.image = [UIImage imageNamed:@"araundImg"];
        for (int i=0; i<[idsForStudent count]; i++) {
            if ([[subj objectForKey:@"Id"] isEqualToString:[idsForStudent objectAtIndex:i]]) {
                cell.checkIcon.image = [UIImage imageNamed:@"CheckIcon"];
                break;
            }
        }
        
        return cell;
    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
        NSString *subj = [[sectionUnivers objectAtIndex:indexPath.row] objectForKey:@"Subject"];
        
        cell.textLabel.text = subj;
        
        return cell;
    }
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    return self.myTableectionTitles;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.editingOFSubjList ? 100 : 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.editingOFSubjList) {
        NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
        NSString *idSub = [[sectionUnivers objectAtIndex:indexPath.row] objectForKey:@"Id"];
        
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        NSMutableArray *idsForStudent = [NSMutableArray arrayWithArray:[usDef objectForKey:@"Student Events IDs"]];
        NSMutableArray *forRemove = [NSMutableArray arrayWithArray:idsForStudent];
        
        int num = [idsForStudent count];
        for(NSString * mID in idsForStudent) {
            if ([idSub isEqualToString:mID]) {
                [forRemove removeObject:mID];
            }
        }
        if ([forRemove count] == num) {
            [forRemove addObject:idSub];
        }
        
        [usDef setObject:[forRemove copy] forKey:@"Student Events IDs"];
        [self.tableView reloadData];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSArray *arrayToFilter = subjectsArray;
    
    if (searchText.length) {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.Subject contains[c] %@", searchText];
        
        NSArray *filteredArray = [arrayToFilter filteredArrayUsingPredicate:bPredicate];
        
        arrayToFilter = filteredArray;
        
    }
    
    [self makeSubjectsDictionary:arrayToFilter];
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    return nil;
}


-(void)makeSubjectsDictionary: (NSArray*)beData {
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *teacher in beData) {
        NSString *name = [teacher objectForKey:@"Subject"];
        NSString *firstLetter = [name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:teacher];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:teacher];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

- (void)addPara {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateParaViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"addSubject"];
    
    [self.navigationController pushViewController:viewController animated:YES];
    
}

- (void)editShedule {
    //tutorialSubjectsOfGroupToSelect
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSString *tutName = @"tutorialSubjectsOfGroupToSelect";
    if (![usDef objectForKey:tutName]) {
        TutorialUniverChooseViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:tutName];
        vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.tutToDel = tutName;
        [self presentViewController:vc animated:NO completion:nil];
    }
    
    if (self.editingOFSubjList) {
        self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        self.editingOFSubjList = NO;
        self.saveEditingView.hidden = YES;
        [self.tableView reloadData];
    }
    else {
        self.editingOFSubjList = YES;
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
        self.saveEditingView.hidden = NO;
        [self.tableView reloadData];
    }
}

- (NSString *)day:(NSString*)date {
    //NSString *dateString = @"01-02-2010";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    // voila!
    dateFromString = [dateFormatter dateFromString:date];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:dateFromString];
    int weekday = [comps weekday];
    
    switch (weekday) {
            
        case 2:
            return @"ПН";
            break;
            
        case 3:
            return @"ВТ";
            break;
            
        case 4:
            return @"СР";
            break;
            
        case 5:
            return @"ЧТ";
            break;
            
        case 6:
            return @"ПТ";
            break;
            
        case 7:
            return @"СБ";
            break;
            
        default:
            
            return @"НД";
            break;
    }
}

-(NSString *)paraNumLatins : (NSString*)num {
    int numOfPara = [num intValue];
    
    switch (numOfPara) {
        case 1:
            return @"I";
            break;
            
        case 2:
            return @"II";
            break;
            
        case 3:
            return @"III";
            break;
            
        case 4:
            return @"IV";
            break;
            
        case 5:
            return @"V";
            break;
            
        case 6:
            return @"VI";
            break;
            
        case 7:
            return @"VII";
            break;
            
        default:
            return @"0";
            break;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)saveEditing:(id)sender {
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    self.editingOFSubjList = NO;
    self.saveEditingView.hidden = YES;
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    NSArray *idsForStudent = [NSArray arrayWithArray:[usDef objectForKey:@"Student Events IDs"]];
    NSArray *events = [NSArray arrayWithArray:[usDef objectForKey:@"ScheduleG"]];
    NSMutableArray *newEvents = [NSMutableArray array];
    NSMutableArray *idsNew = [NSMutableArray array];
    NSMutableArray *idsToRemove = [NSMutableArray array];
    
    for (NSDictionary *obj in events) {
        NSString *idInEvent = [obj objectForKey:@"Id"];
        [idsToRemove addObject:idInEvent];
    }
    
    for (NSDictionary *obj in events) {
        NSString *idInEvent = [obj objectForKey:@"Id"];
        
        for (int i=0; i<[idsForStudent count]; i++) {
            if ([idInEvent isEqualToString:[idsForStudent objectAtIndex:i]]) {
                [newEvents addObject:obj];
                [idsNew addObject:idInEvent];
                [idsToRemove removeObject:idInEvent];
            }
        }
    }
    
    NSString *stID = [usDef objectForKey:@"Student ID"];
    
    [self.mnetworkManager addEventToStudentWithStudentID:stID events:[idsNew copy] withCompletion:^(NSInteger added) {
        
    } failure:^(NSError *error) {
        
    }];
    
    [self.mnetworkManager removeEventsToStudentWithStudentID:stID events:idsToRemove withCompletion:^(NSInteger removed) {
        
    } failure:^(NSError *error) {
        
    }];
    
    [usDef setObject:[newEvents copy] forKey:@"Schedule"];
    [self.tableView reloadData];
}

@end
