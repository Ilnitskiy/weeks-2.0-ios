//
//  SaveResultsTableViewCell.h
//  Weeks
//
//  Created by Max Ilnitskiy on 31.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaveResultsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *saveLabel;

@end
