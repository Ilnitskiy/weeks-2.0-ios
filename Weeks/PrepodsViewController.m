//
//  PrepodsViewController.m
//  Weeks
//
//  Created by Max Ilnitskiy on 23.01.17.
//  Copyright © 2017 Max Ilnitskiy. All rights reserved.
//

#import "PrepodsViewController.h"
#import "NetworkManager.h"

@interface PrepodsViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBr;
@property (strong, nonatomic) NSDictionary *myTable;
@property (strong, nonatomic) NSArray *myTableectionTitles;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@property (strong, nonatomic) NetworkManager *mnetworkManager;
- (IBAction)closeBtn:(id)sender;
- (IBAction)addBtn:(id)sender;


@end

@implementation PrepodsViewController

{
    NSArray *subjectsArray;
    UIView *headerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = TRUE;
    
    [indicator startAnimating];

    headerView = self.tableView.tableHeaderView;
    self.tableView.tableHeaderView = nil;
    
    self.navigationItem.title = @"Викладачі";
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:42.0/255.0 green:85.0/255.0 blue:115.0/255.0 alpha:1.0]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    //self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:99.0/255.0 green:139.0/255.0 blue:165.0/255.0 alpha:1.0];
    //self.navigationController.navigationBar.translucent = NO;
    //    [self.navigationController.navigationBar setTitleTextAttributes:
    //     @{NSForegroundColorAttributeName:[UIColor whiteColor],
    //       NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Regular" size:21]}];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    self.univerID = [usDef objectForKey:@"Univer ID"];
    
    self.mnetworkManager = [[NetworkManager alloc]init];
    
    [self.mnetworkManager getUniverStuffWithUniverID:self.univerID completion:^(NSDictionary *stuf) {
        NSArray *subjArr = [NSArray arrayWithArray:[stuf objectForKey:@"mentors"]];
        subjectsArray = subjArr;
        [self makeSubjectsDictionary:subjArr];
        
        [indicator stopAnimating];
    } failure:^(NSError *error) {
        [indicator stopAnimating];
    }];
    
    //
    //    NSDictionary *tempData  = @{
    //                      @"А":@[@"Алгебра",@"Аналітична геометрія"],
    //                      @"Б":@[@"Біологія"],
    //                      @"Д":@[@"Дискретна метематика"],
    //                      @"К":@[@"Креслення",@"Критичне мислення"],
    //                      @"Т":@[@"Топографія",@"Теорія електричних кіл"],
    //                      @"Ф":@[@"Фізика",@"Фізична підготовка"],
    //                      };
    //
    //    self.myTableectionTitles = [[tempData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    //    NSString *str = [self.myTableectionTitles lastObject];
    //    NSMutableArray *lastSection = [[tempData objectForKey:str] mutableCopy];
    //    [lastSection addObject:@"weeksLastCell"];
    //
    //    NSMutableDictionary *dictWithCell = [tempData mutableCopy];
    //
    //    [dictWithCell setObject:lastSection forKey:[self.myTableectionTitles lastObject]];
    //
    //    self.myTable=[dictWithCell copy];
    //
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (IBAction)addButtonAction:(id)sender {
    [self showAddButtonWithText:self.searchBr.text];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(showKeyboard) withObject:nil afterDelay:0.1];
}

- (void) showKeyboard
{
    [self.searchBr becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [self.myTableectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return [self.myTableectionTitles objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [self.myTable objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    NSString *subj = [sectionUnivers objectAtIndex:indexPath.row];
    
    //    if ([subj isEqualToString:@"weeksLastCell"]) {
    //        cell.imageView.hidden = YES;
    //        cell.textLabel.text =@"";
    //        //cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    //        return cell;
    //    }
    //    else {
    //
    //cell.imageView.hidden = NO;
    cell.textLabel.text = subj;
    //cell.imageView.image = [UIImage imageNamed:@"araundImg"];
    //cell.imageView.clipsToBounds = YES;
    //cell.imageView.layer.cornerRadius = 9.0;
    //
    //    CGFloat headerViewHeight = [self.tableView headerViewForSection:0].frame.size.height;
    
    return cell;
    //    }
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    
    return self.myTableectionTitles;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"CheckIcon"];
    NSString *sectionTitle = [self.myTableectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionUnivers = [self.myTable objectForKey:sectionTitle];
    NSString *subj = [sectionUnivers objectAtIndex:indexPath.row];
    
    NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
    [usDef setObject:subj forKey:@"teachernameToPredmet"];
    [usDef synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    NSArray *arrayToFilter = subjectsArray;
    
    if (searchText.length) {
        self.tableView.tableHeaderView = headerView;
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[c] %@", searchText];
        
        NSArray *filteredArray = [arrayToFilter filteredArrayUsingPredicate:bPredicate];
        
        arrayToFilter = filteredArray;
    } else {
        self.tableView.tableHeaderView = nil;
    }
    
    self.headerLabel.text = searchText;
    
    [self makeSubjectsDictionary:arrayToFilter];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (section == [tableView numberOfSections] - 1) {
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }
    return nil;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (section == [tableView numberOfSections] - 1) {
//        return 44;
//    }
//    return 0.01;
//}

-(void)makeSubjectsDictionary: (NSArray*)beData {
    
    NSMutableDictionary *abcUnivers = [[NSMutableDictionary alloc]init];
    
    for (NSDictionary *teacher in beData) {
        NSString *name = [teacher objectForKey:@"name"];
        NSString *firstLetter = [name substringToIndex:1];
        
        NSString *upFL = [firstLetter uppercaseString];
        
        if ([abcUnivers valueForKey:upFL]) {
            NSMutableArray *arrayNames = [NSMutableArray arrayWithArray:[abcUnivers valueForKey:upFL]];
            [arrayNames addObject:name];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
        else {
            NSMutableArray *arrayNames = [[NSMutableArray alloc]init];
            [arrayNames addObject:name];
            [abcUnivers setObject:arrayNames forKey:upFL];
        }
    }
    
    self.myTable = abcUnivers;
    self.myTableectionTitles = [[self.myTable allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (IBAction)podilitysBtnAct:(id)sender {
    
}

- (IBAction)nextBtn:(id)sender {
    
}

- (IBAction)closeBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addBtn:(id)sender {
    [self showAddButtonWithText:nil];
}

- (void)showAddButtonWithText:(NSString *)text {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Новий викладач"
                                                                              message:@"Введіть ПІБ викладача"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Наприклад: Лінчевський Ігор Володимирович";
        textField.text = text;
        textField.textColor = [UIColor blackColor];
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleNone;
        textField.secureTextEntry = NO;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Зберегти" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * mailfield = textfields[0];
        NSLog(@"%@",mailfield.text);
        NSUserDefaults *usDef = [NSUserDefaults standardUserDefaults];
        if (mailfield.text) {
            [usDef setObject:mailfield.text forKey:@"teachernameToPredmet"];
            [usDef synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Відмінити" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
