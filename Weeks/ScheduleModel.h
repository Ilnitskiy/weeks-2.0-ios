//
//  ScheduleModel.h
//  Weeks
//
//  Created by Max Ilnitskiy on 25.12.16.
//  Copyright © 2016 Max Ilnitskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduleModel : NSObject
@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) NSString *Subject;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *Mentor;
@property (strong, nonatomic) NSString *Lecture_hall;
@property (strong, nonatomic) NSString *Latitude;
@property (strong, nonatomic) NSString *Number;
@property (strong, nonatomic) NSArray *Time;
@property (strong, nonatomic) NSArray *Dates;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
